-/*
This query is meant to support https://pinnacol.atlassian.net/browse/IDSSDS-2006 eda for rtw


*/
with industry_data as
 (select froi.claim_num,
         mcb.manual_class_num,
         mcb.industry_group_code,
         mcb.manual_class_type_code,
         mcb.manual_class_descr
    from first_report_of_injury froi, manual_class_base mcb
   where froi.manual_class_num = mcb.manual_class_num
     and froi.accident_date between mcb.manual_class_rate_eff_date and
         NVL(mcb.manual_class_rate_exp_date, sysdate + 1)
     and rate_file_seq_num =
         (select min(rate_file_seq_num)
            from manual_class_base m2
           where mcb.manual_class_num = m2.manual_class_num
             and sysdate between m2.manual_class_rate_eff_date and
                 NVL(m2.manual_class_rate_exp_date, sysdate + 1))),

policy_claim_count_total as
 (select count(*) clm_cnt_on_policy, c.policy_num
    from claim c
   group by c.policy_num),
rtw_policy as
 (SELECT count(rr.pinnacol_num) rtw_policy_count,
         rr.pinnacol_num as policy_num
    FROM wcis_dba.rtw_request rr
   WHERE rr.pinnacol_type_code = 'POL'
   GROUP BY pinnacol_num),
max_rtw_date as
 (SELECT max(rr.date_iw_returned_to_work) max_rtw_date,
         rr.pinnacol_num as claim_num
    FROM wcis_dba.rtw_request rr
   WHERE rr.pinnacol_type_code = 'CLM'
   GROUP BY pinnacol_num),
--rma
rtw_claim as
 (SELECT count(rr.pinnacol_num) rtw_claim_count,
         rr.pinnacol_num as claim_num
    FROM wcis_dba.rtw_request rr
   WHERE rr.pinnacol_type_code = 'CLM'
   GROUP BY pinnacol_num),
last_indemnity_payment_date as
 (select max(end_period_date) as last_ind_payment_date, cft.claim_num
    from claim_financial_trans cft
    where and comp_code in ('TT','TP')
    and 0 = (select count(*) from claim_financial_trans cft2
              where cft2.claim_num = cft.claim_num
              and comp_code in ('PP','PT','PERM'))
   group by cft.claim_num),
list_benefits as
 (select claim_num,
         listagg(benefit_category_code || ' ') within group(order by benefit_period_seq_num) as benefit_category_codes
    from benefit_period bp
   group by claim_num),
all_icd as
 (select mbs.claim_num claim_num,
         substr(listagg(NVL(mbs.ICD9_CODE1, '') || ' ' ||
                        NVL(mbs.ICD9_CODE2, '') || ' ' ||
                        NVL(mbs.ICD9_CODE3, '') || ' ' ||
                        NVL(mbs.ICD9_CODE4, ''),
                        ', ') WITHIN GROUP(ORDER BY mbs.bill_seq_num),
                1,
                3999) as ALL_ICD_CODES,
         min(mbs.RECV_DATE) as min_icd_date
    from med_bill_summary mbs, claim c
   where mbs.claim_num = c.claim_num
     and mbs.current_flag = 'Y'
     and mbs.enter_date <= c.accident_date + 30
     and mbs.enter_date <= (select min(action_plan_date)
                              from rn_action_plan r
                             where r.claim_num = c.claim_num
                             group by c.claim_num)
   group by mbs.claim_num),
all_billing_codes as
 (select mbs.claim_num claim_num,
         substr(listagg(mbd.billed_procedure_prefix_code ||
                        mbd.billed_procedure_code || ',') WITHIN
                GROUP(ORDER BY mbd.detail_seq_num),
                1,
                3999) as ALL_BILLED_PROC_CODES,
         min(mbs.RECV_DATE) as min_cpt_date
    from med_bill_summary mbs, med_bill_detail mbd, claim c
   where mbs.bill_seq_num = mbd.bill_seq_num
     and mbs.version_num = mbd.version_num
     and mbs.claim_num = c.claim_num
     and mbs.enter_date <= c.accident_date + 30
     and mbs.enter_date <= (select min(action_plan_date)
                              from rn_action_plan r
                             where r.claim_num = c.claim_num
                             group by c.claim_num)
     and mbs.current_flag = 'Y'
   group by mbs.claim_num)
select c.claim_num,
       c.policy_num,
       c.accident_date,
       ct.gross_incurred_ind,
       ct.gross_incurred_med,
       ct.gross_incurred_total,
       i.injury_code,
       i.injury_sev_code,
       ca.entry_of_app_date,
       ai.ALL_ICD_CODES,
       ai.min_icd_date,
       abc.ALL_BILLED_PROC_CODES,
       abc.min_cpt_date,
       lipd.last_ind_payment_date,
       lb.benefit_category_codes,
       rc.rtw_claim_count,
       rp.rtw_policy_count,
       id.manual_class_num,
       id.industry_group_code,
       id.manual_class_type_code,
       id.manual_class_descr,
       p.policy_orig_eff_date,
       pcct.clm_cnt_on_policy
  from claim c
 inner join injury i
    on i.claim_num = c.claim_num
   and i.primary_injury_flag = 'Y'
 inner join claim_totals ct
    on ct.claim_num = c.claim_num
 inner join policy p
    on p.policy_num = c.policy_num
  left join claim_attorney ca
    on ca.claim_num = c.claim_num
   and ca.primary_flag = 'Y'
  left join all_icd ai
    on ai.claim_num = c.claim_num
  left join all_billing_codes abc
    on abc.claim_num = c.claim_num
  left join list_benefits lb
    on lb.claim_num = c.claim_num
  left join last_indemnity_payment_date lipd
    on lipd.claim_num = c.claim_num
  left join max_rtw_date mrd
    on mrd.claim_num = c.claim_num
  left join rtw_claim rc
    on rc.claim_num = c.claim_num
  left join rtw_policy rp
    on rp.policy_num = c.policy_num
  left join policy_claim_count_total pcct
    on pcct.policy_num = c.policy_num
  left join industry_data id
    on id.claim_num = c.claim_num
 where EXTRACT(YEAR from c.accident_date) >= 2019
 --  and c.claim_num in
 --      (3997019, 10059791, 10060014, 10059994, 10060103, 3878432, 3874561)
--3874561 fatal
