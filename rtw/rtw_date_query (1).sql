/* 3 possible return to work dates 
1. claim.return_to_work this is populated at intake
2. beneifit_period.beneift_end_date this is the end date of indemnity payments.  The thought is that they may be going back to work after we stop paying them. 
   I added the benefit_category_code because sometimes an IW can be working TPD at modified duty or just part time.  There may be something similar with PPD. 
3. rtw_requests.date_iw_returned_to_work.  This is populated when an RTW specialist is involved with a calim.

   Claims with no benefit period or 0 indemnity paid are med only claims and wouldn't have a rtw date.    s
*/

with 
 benefit_period_end_date as (select bp.claim_num, bp.benefit_end_date, bp.benefit_category_code
                                 from benefit_period             bp
                                     ,claim_benefit_notification cbn
                                     ,benefits_notification      bn
                                 where bn.benefits_notification_seq_num = cbn.benefits_notification_seq_num
                                 and cbn.benefit_period_seq_num       = bp.benefit_period_seq_num 
                                 and bn.current_notification_flag = 'Y'
                                 and bp.benefit_category_code != 'MISC'
                                 and bp.benefit_period_seq_num = (select max(bp2.benefit_period_seq_num)
                                                                  from benefit_period bp2
                                                                  where bp2.claim_num =  bp.claim_num
                                                                  and bp.benefit_category_code in ('TTD','TPD'))
           )  ,
 benefit_category_code as (select bp3.claim_num, bp3.benefit_category_code                                                     
                                  from benefit_period             bp3
                                     ,claim_benefit_notification cbn2
                                     ,benefits_notification      bn2
                                 where bn2.benefits_notification_seq_num = cbn2.benefits_notification_seq_num
                                 and cbn2.benefit_period_seq_num       = bp3.benefit_period_seq_num 
                                 and bn2.current_notification_flag = 'Y'
                                 and bp3.benefit_category_code != 'MISC'
                                 and bp3.benefit_period_seq_num = (select max(bp4.benefit_period_seq_num)
                                                                  from benefit_period bp4
                                                                  where bp4.claim_num =  bp3.claim_num))                          
  select c.claim_num
        , c.claim_type_code
       , c.accident_date
       , c.return_work_date
       , bped.benefit_end_date 
       , bcc.benefit_category_code
       , ct.gross_paid_ind indemnity_paid 
       , ct.gross_paid_med med_paid 
  from claim c
       ,benefit_period_end_date bped     
       ,benefit_category_code bcc
       ,claim_totals ct
  where extract(YEAR from accident_date) = 2018
  and c.claim_type_code not in ('CC','MCA')
  and c.claim_status_code = 'Z'
  and c.claim_num = ct.claim_num
  and c.claim_num = bped.claim_num(+)
  and c.claim_num = bcc.claim_num(+)
  --and ct.gross_paid_ind > 0 
  order by bped.benefit_category_code
  
                  
                
