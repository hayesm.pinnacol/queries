/*

[hayesm@lnxwlfrftden33 prd]$ ls rw*
rw100r1.rdf  rw105s.fmx   rw115s.fmb  rw125s.fmb  rw135s.fmb   rw145s.fmb
rw100r8.rdf  rw110s.fmb   rw115s.fmx  rw125s.fmx  rw135s.fmx   rw145s.fmx
rw100s.fmb   rw110s.fmx   rw120s.err  rw130s.fmb  rw140s.fmb   rw150s.fmb
rw100s.fmx   rw115r2.rdf  rw120s.fmb  rw130s.fmx  rw140s.fmx   rw150s.fmx
rw105s.fmb   rw115r5.rdf  rw120s.fmx  rw135s.err  rw145r1.rdf


1032  RW100S	RTW Main Menu
1033	RW120S	RTW Request
1034	RW150S	RTW Report Menu


*/


--high priority block rw100s 
SELECT /*+ all_rows */
 r.request_seq_num,
 r.target_due_date,
 r.create_date,
 r.rtw_specialist_user_name,
 r.did_iw_return_to_work_flag,
 r.rtw_reason_code,
 r.request_status_code,
 pk90200_rtw.get_target_city(r.pinnacol_num, r.pinnacol_type_code) city,
 pk90200_rtw.get_target_name(r.pinnacol_num, r.pinnacol_type_code) target_name,
 pk90200_rtw.get_target_name_2(r.pinnacol_num, r.pinnacol_type_code) target_name_2,
 DECODE(requestor_user_name,
        'RPN',
        'Rehab Provider Network',
        INITCAP(we.employee_first_name || ' ' || employee_last_name)) requestor_name,
 r.pinnacol_num,
 r.pinnacol_type_code,
 ptc.code_descr pinnacol_type_code_descr,
 TO_CHAR(NVL(pk90200_rtw.get_pct_complete(r.request_seq_num), 0), '999') || '%' pct_complete,
 decode(sp90540_threat_exists(r.pinnacol_num, r.pinnacol_type_code),
        'Y',
        '!',
        null) threat_flag
  FROM rtw_request r, wcis_employee we, rtw_pinnacol_type_code ptc
 WHERE r.requestor_user_name = we.user_name(+)
   AND r.pinnacol_type_code = ptc.code) request
 
 
 --new request query rm100s
 SELECT /*+ all_rows */
  r.request_seq_num,
  r.target_due_date,
  r.create_date,
  DECODE(requestor_user_name, 'RPN', 'None', we.sbu_team_code) sbu_team_code,
  DECODE(requestor_user_name,
         'RPN',
         'Rehab Provider Network',
         INITCAP(employee_first_name || ' ' || employee_last_name)) requestor_name,
  r.pinnacol_num,
  r.pinnacol_type_code,
  ptc.code_descr pinnacol_type_code_descr,
  request_status_code,
  r.rtw_specialist_user_name,
  r.ref_table,
  r.ref_seq_num,
  decode(sp90540_threat_exists(r.pinnacol_num, r.pinnacol_type_code),
         'Y',
         '!',
         null) threat_flag
   FROM rtw_request r, wcis_employee we, rtw_pinnacol_type_code ptc
  WHERE r.requestor_user_name = we.user_name(+)
    AND r.pinnacol_type_code = ptc.code
    
    
    
--rn120s rtw_request block 
SELECT request_seq_num,
       pinnacol_num,
       pinnacol_type_code,
       ptc.code_descr pinnacol_type_code_descr,
       create_user_name,
       r.create_date,
       parent_request_seq_num,
       loc_seq_num,
       savings_amt,
       request_status_code,
       rsc.code_descr request_status_code_descr,
       full_duty_work_release_date,
       rtw_specialist_user_name,
       INITCAP(we1.employee_first_name || ' ' || we1.employee_last_name) rtw_specialist_full_name,
       we1.phone_ext rtw_specialist_phone,
       requestor_user_name,
       INITCAP(we2.employee_first_name || ' ' || we2.employee_last_name) requestor_full_name,
       we2.phone_ext requestor_phone,
       workplan_setup_date,
       target_due_date,
       request_complete_date,
       rule_effective_date,
       rule_presented_date,
       rule_delivery_type_code,
       rule_name_of_doctor,
       rule_job_tasks,
       did_iw_return_to_work_flag,
       update_user_name,
       r.update_date,
       contact_name,
       contact_phone,
       contact_email,
       notes,
       modified_duty_rating_score,
       requestor_instructions
  FROM rtw_request             r,
       wcis_employee           we1,
       wcis_employee           we2,
       rtw_pinnacol_type_code  ptc,
       rtw_request_status_code rsc
 WHERE r.rtw_specialist_user_name = we1.user_name
   AND r.requestor_user_name = we2.user_name
   AND r.pinnacol_type_code = ptc.code
   AND r.request_status_code = rsc.code


--rm120s rtw_reqeust_service 
SELECT request_service_seq_num,
       request_seq_num,
       sc.code                 service_code,
       sc.code_descr           service_code_descr,
       base_service_flag,
       status_text,
       sc.sort_order,
       sc.pinnacol_type_code
  FROM rtw_request_service rs, rtw_service_code sc
 WHERE sc.code = rs.service_code(+)
   and 0 = request_seq_num(+)
   and sc.expired_flag <> 'Y'
   and sc.code <> '31MOD'
   and ((substr(user, 5) in (select user_name from rtw_specialist)) or
       (substr(user, 5) not in (select user_name from rtw_specialist) and
       sc.code != 'RTW'))
  
 --lov from rw150s rpt team 
SELECT ou.code, ou.code_descr, '' blank, '' blank2
  FROM org_unit ous
 where ou.org_level_code = 'TEAM'
   and ou.business_team_flag = 'Y'
   and ou.active_flag = 'Y'
UNION
SELECT 'ALL', 'All Teams', '' blank, '' blank2
  from dual
minus
select decode(:program_name, 'RW100R7', 'ALL', 'RW115R1', 'ALL', null),
       decode(:program_name,
              'RW100R7',
              'All Teams',
              'RW115R1',
              'All Teams',
              null),
       '' blank,
       '' blank2
  from dual
  
--lov from rw150s rpt individual
select employee_last_name || ' ' || employee_first_name name,
       we.user_name,
       '' blank
  from wcis_employee we
 where we.employee_status_code = 'PA'
   and ((:rtw_report.program_name = 'RW100R4' and
       pk_security.has_role_qry(user_name, 'ECC-9') = 1) or
       employee_class_code = employee_class_code)
union
select 'ALL', 'ALL', '' blank
  from dual
 where :rtw_report.program_name = 'RW100R4'



SELECT initcap(we.employee_last_name || ', ' || we.employee_first_name) RTW_SPECIALIST_USER_NAME,
       c.claim_unit_code SBG_TEAM,
       SUM(savings) savings
  FROM (SELECT a.claim_num,
               C.BENEFIT_START_DATE,
               NVL(C.BENEFIT_END_DATE, SYSDATE),
               E.AWW_RATE,
               ROUND((((NVL(C.BENEFIT_END_DATE, SYSDATE) -
                     C.BENEFIT_START_DATE) + 1) / 7) * E.AWW_RATE * (2 / 3),
                     2) savings
          FROM BENEFITS_NOTIFICATION        A,
               CLAIM_BENEFIT_NOTIFICATION   B,
               BENEFIT_PERIOD               C,
               BENEFIT_PERIOD_INCOME_SOURCE D,
               CLAIMANT_INCOME_SOURCE       E
         WHERE A.COMMIT_STATUS_CODE = 'C'
           AND A.CURRENT_NOTIFICATION_FLAG = 'Y'
           AND B.BENEFITS_NOTIFICATION_SEQ_NUM =
               A.BENEFITS_NOTIFICATION_SEQ_NUM
           AND C.BENEFIT_PERIOD_SEQ_NUM = B.BENEFIT_PERIOD_SEQ_NUM
           AND C.CLAIM_NUM = A.CLAIM_NUM
           AND D.BENEFIT_PERIOD_SEQ_NUM = C.BENEFIT_PERIOD_SEQ_NUM
           AND E.INCOME_SOURCE_SEQ_NUM = D.INCOME_SOURCE_SEQ_NUM
           AND NVL(E.POST_INJURY_FLAG, 'N') = 'Y'
           AND a.claim_num IN
               (SELECT pinnacol_num
                  FROM rtw_request rq
                 WHERE pinnacol_type_code = 'CLM'
                   AND c.benefit_start_date >= rq.create_date
                   AND rq.request_status_code = 'COMP'
                   AND rq.request_complete_date BETWEEN :p_from_date AND
                       nvl(:p_thru_date, sysdate + 1)
                   AND EXISTS
                 (SELECT 'x'
                          FROM rtw_request_service rs
                         WHERE service_code IN ('RIX', 'PT')
                           AND rs.request_seq_num = rq.request_seq_num))) claim_savings,
       rtw_request rr,
       claim c,
       claim_unit_code cuc,
       wcis_employee we
 WHERE claim_savings.claim_num = c.claim_num
   AND c.claim_num = rr.pinnacol_num
   AND RR.REQUEST_SEQ_NUM =
       (SELECT MIN(REQUEST_SEQ_NUM)
          FROM RTW_REQUEST
         WHERE PINNACOL_NUM = C.CLAIM_NUM
           AND PINNACOL_TYPE_CODE = 'CLM')
   and rr.rtw_specialist_user_name = we.user_name
   AND rr.pinnacol_type_code = 'CLM'
   AND rr.request_status_code = 'COMP'
   AND rr.request_complete_date BETWEEN :p_from_date AND
       nvl(:p_thru_date, sysdate + 1)
   AND c.claim_unit_code = cuc.codes

--rw115s rtw receptiveness by group, agen 
SELECT pp.policy_num,
       'For Policy' claim_num,
       pp.underwriter_user_name,
       initcap(we.employee_last_name || ' ,' || we.employee_first_name) underwriter,
       pp.claim_unit_code,
       INITCAP(we2.employee_last_name || ' ,' || we2.employee_first_name) rtw_specialist,
       rw.from_date from_date,
       rw.thru_date,
       decode(rw.willingness_flag, 'Y', 'Yes', 'N', 'No', 'Unknown') willingness,
       rw.note,
       pp.group_assoc_code || '-' || initcap(gac.code_descr) group_assoc,
       initcap(a.agent_name) agent
  FROM rtw_willingness        rw,
       policy_period          pp,
       wcis_employee          we,
       wcis_employee          we2,
       agent_policy_period    app,
       agent                  a,
       group_association_code gac
 WHERE rw.pinnacol_num = pp.policy_num
   AND rw.pinnacol_type_code = 'POL'
   AND rw.from_date between :p_from_date and :p_thru_date
   AND SYSDATE BETWEEN pp.policy_period_eff_date AND
       NVL(pp.policy_period_exp_date, SYSDATE + 1)
   and we.user_name = pp.underwriter_user_name
   and we2.user_name = rw.create_user_name
   AND app.POLICY_NUM = pp.POLICY_NUM
   AND app.POLICY_PERIOD_EFF_DATE = pp.POLICY_PERIOD_EFF_DATE
   AND NVL(app.history_record_flag, 'N') <> 'Y'
   AND pp.policy_period_eff_date BETWEEN app.agent_eff_date AND
       app.agent_exp_date
   AND a.AGENT_NUM = app.AGENT_NUM
   AND pp.group_assoc_code = gac.code
   AND pp.group_assoc_code = nvl(:p_group_code, pp.group_assoc_code)
UNION
SELECT c.policy_num,
       to_char(c.claim_num) || ' (' || c.adjuster_code || ')',
       pp.underwriter_user_name,
       initcap(we.employee_last_name || ' ,' || we.employee_first_name) underwriter,
       pp.claim_unit_code,
       INITCAP(we2.employee_last_name || ' ,' || we2.employee_first_name) rtw_specialist,
       trunc(rw.from_date) from_date,
       trunc(rw.thru_date),
       decode(rw.willingness_flag, 'Y', 'Yes', 'N', 'No', 'Unknown') willingness,
       rw.note,
       pp.group_assoc_code || '-' || initcap(gac.code_descr) group_assoc,
       initcap(a.agent_name) agent
  FROM rtw_willingness        rw,
       policy_period          pp,
       claim                  c,
       wcis_employee          we,
       wcis_employee          we2,
       agent_policy_period    app,
       agent                  a,
       group_association_code gac
 WHERE rw.pinnacol_num = c.claim_num
   AND rw.pinnacol_type_code = 'CLM'
   AND rw.from_date between :p_from_date and :p_thru_date
   AND c.policy_num = pp.policy_num
   AND c.policy_period_eff_date = pp.policy_period_eff_date
   and we.user_name = pp.underwriter_user_name
   and we2.user_name = rw.create_user_name
   AND app.POLICY_NUM = pp.POLICY_NUM
   AND app.POLICY_PERIOD_EFF_DATE = pp.POLICY_PERIOD_EFF_DATE
   AND NVL(app.history_record_flag, 'N') <> 'Y'
   AND pp.policy_period_eff_date BETWEEN app.agent_eff_date AND
       app.agent_exp_date
   AND a.AGENT_NUM = app.AGENT_NUM
   AND pp.group_assoc_code = gac.code
   AND pp.group_assoc_code = nvl(:p_group_code, pp.group_assoc_code)
UNION
SELECT c.policy_num,
       to_char(c.claim_num) || ' (' || c.adjuster_code || ')',
       pp.underwriter_user_name,
       initcap(we.employee_last_name || ' ,' || we.employee_first_name) underwriter,
       pp.claim_unit_code,
       INITCAP(we2.employee_last_name || ' ,' || we2.employee_first_name) rtw_specialist,
       trunc(rw.create_date) from_date,
       sysdate,
       to_char(rw.receptiveness_rating) willingness,
       nvl(nvl(decode(rw.ttd_success_reason,
                      'FJO',
                      'Formal Job Offer',
                      'FDR',
                      'Full Duty Release',
                      'MMI',
                      'MMI',
                      'RTD',
                      'RTW Documentation'),
               decode(rw.ttd_clm_failure_reason,
                      'DENY',
                      'Claim Denied',
                      'STIP',
                      'Claim Settled',
                      'MMI',
                      'MMI',
                      'MUW',
                      'Medically Unable to Work',
                      'MNC',
                      'Medically Non-Compliant')),
           decode(rw.ttd_pol_failure_reason,
                  'CLOSE',
                  'Business Closed',
                  'MODNA',
                  'Modified Duty Not Available',
                  'RMOD',
                  'Refused Modified Duty',
                  'SE',
                  'Seasonal Employee',
                  'TERM',
                  'Terminated',
                  'WAE',
                  'Working for Another Employer')) || ' - ' ||
       (select note_text
          from rtw_request_note
         where request_seq_num = rw.request_seq_num
           and note_text not like 'Policyholders receptiveness%'
           and note_seq_num =
               (select max(note_seq_num)
                  from rtw_request_note
                 where request_seq_num = rw.request_seq_num
                   and note_text not like 'Policyholders receptiveness%')) note,
       pp.group_assoc_code || '-' || initcap(gac.code_descr) group_assoc,
       initcap(a.agent_name) agent
  FROM rtw_request_outcome    rw,
       policy_period          pp,
       claim                  c,
       wcis_employee          we,
       wcis_employee          we2,
       agent_policy_period    app,
       agent                  a,
       group_association_code gac,
       rtw_request            rr
 WHERE rw.request_seq_num = rr.request_seq_num
   AND rr.pinnacol_num = c.claim_num
   AND rr.pinnacol_type_code = 'CLM'
   AND rw.create_date between :p_from_date and :p_thru_date
   AND c.policy_num = pp.policy_num
   AND c.policy_period_eff_date = pp.policy_period_eff_date
   and we.user_name = pp.underwriter_user_name
   and we2.user_name = rw.create_user
   AND app.POLICY_NUM = pp.POLICY_NUM
   AND app.POLICY_PERIOD_EFF_DATE = pp.POLICY_PERIOD_EFF_DATE
   AND NVL(app.history_record_flag, 'N') <> 'Y'
   AND pp.policy_period_eff_date BETWEEN app.agent_eff_date AND
       app.agent_exp_date
   AND a.AGENT_NUM = app.AGENT_NUM
   AND pp.group_assoc_code = gac.code
   AND pp.group_assoc_code = nvl(:p_group_code, pp.group_assoc_code)
 ORDER BY agent, from_date DESC

--rtw expenses rw115r5
SELECT expense_date,
       pinnacol_num,
       pinnacol_type_code,
       expense_note,
       SUM(DECODE(red.expense_type_code, 'MILE', red.expense_amt, 0)) mile_$,
       SUM(DECODE(red.expense_type_code, 'MILE', red.quantity, 0)) miles,
       SUM(DECODE(red.expense_type_code, 'AIR', red.expense_amt, 0)) air,
       SUM(DECODE(red.expense_type_code, 'RENT', red.expense_amt, 0)) rent,
       SUM(DECODE(red.expense_type_code, 'LODGE', red.expense_amt, 0)) lodge,
       SUM(DECODE(red.expense_type_code, 'MEAL', red.expense_amt, 0)) meal,
       SUM(DECODE(red.expense_type_code, 'ENT', red.expense_amt, 0)) ent,
       SUM(DECODE(red.expense_type_code,
                  'OTH',
                  red.expense_amt,
                  'PRINT',
                  red.expense_amt,
                  'PHONE',
                  red.expense_amt,
                  'PARK',
                  red.expense_amt,
                  0)) oth
  FROM rtw_request                r,
       rtw_request_expense        re,
       rtw_request_expense_detail red
 WHERE r.request_seq_num = re.request_seq_num
   AND r.rtw_specialist_user_name = :cp_user_name
   AND re.request_expense_seq_num = red.request_expense_seq_num
   AND re.expense_date between :p_from_date and nvl(:p_thru_date, sysdate)
 GROUP BY expense_date, pinnacol_num, pinnacol_type_code, expense_note
UNION
SELECT TO_DATE(NULL) expense_date,
       TO_NUMBER(NULL) pinnacol_num,
       TO_CHAR(NULL) pinnacol_type_code,
       LPAD('Mileage Reimbursement Rate: $0' || :CF_REIMBURSE_RATE, 140) expense_note,
       TO_NUMBER(NULL) mile_$,
       :CF_MILES_TOT miles,
       TO_NUMBER(NULL) air,
       TO_NUMBER(NULL) rent,
       TO_NUMBER(NULL) lodge,
       TO_NUMBER(NULL) meal,
       TO_NUMBER(NULL) ent,
       TO_NUMBER(NULL) oth
  FROM DUAL
