
with policy_data as
 (select c.claim_num,
         p.policy_num,
         p.policy_name,
         count(*) OVER(partition by c.claim_num) as clm_cnt_on_policy
    from claim c, policy_period pp, policy p
   where c.policy_num = p.policy_num
     and p.policy_num = pp.policy_num
     and c.accident_date between pp.policy_period_eff_date and
         pp.policy_period_exp_date),
industry_data as
 (select froi.claim_num,
         mcb.manual_class_num,
         mcb.industry_group_code,
         mcb.manual_class_type_code,
         mcb.manual_class_descr
    from first_report_of_injury froi, manual_class_base mcb
   where froi.manual_class_num = mcb.manual_class_num
     and froi.accident_date between mcb.manual_class_rate_eff_date and
         NVL(mcb.manual_class_rate_exp_date, sysdate + 1)
     and rate_file_seq_num =
         (select min(rate_file_seq_num)
            from manual_class_base m2
           where mcb.manual_class_num = m2.manual_class_num
             and sysdate between m2.manual_class_rate_eff_date and
                 NVL(m2.manual_class_rate_exp_date, sysdate + 1))),
first_nurse_referral_date as
 (select min(action_plan_date) as min_date_of_nurse_ref, r.claim_num
    from rn_action_plan r, claim c
   where r.claim_num = c.claim_num
   group by r.claim_num),
first_indemnity_payment_date as
 (select min(trans_date) as first_ind_payment_date, cft.claim_num
    from claim_financial_trans cft
   group by cft.claim_num),
denial_correspond as
 (select count(*) as denial_correspond_cnt, cc.claim_num
    from claim_correspond cc
   where cc.correspond_code in ('C328', 'C333', 'C334')
   group by cc.claim_num),
settlements as
 (select cft.claim_num,
         min(cft.trans_date) first_settlement_date,
         sum(cft.net_trans_amt) settlement_sum,
         count(*) num_of_settlements_on_claim
    from claim_financial_trans cft
   where cft.trans_code = 'T'
     and cft.trans_status_code = 'B'
   group by cft.claim_num),
pinnacol_eoa as
 (select ceh.claim_num,
         min(ceh.create_date) min_pinnacol_eoa_date,
         max(ceh.create_date) max_pinnacol_eoa_date,
         count(*) num_pinnacol_eoa_events
    from cm_event_hierarchy ceh
   where ceh.cm_event_type_id = 35
   group by ceh.claim_num),
siu_ref as
 (select siu.pinnacol_num,
         min(siu.create_date) min_siu_referral,
         max(siu.create_date) max_siu_referral,
         count(*) num_siu_referrals
    from siu_referral siu
   group by siu.pinnacol_num),
claimant_atty_primary as
 (select ca.claim_num, min(ca.create_date) min_ca_date_primary
    from claim_attorney ca
   where primary_flag = 'Y'
   group by ca.claim_num),
claimant_atty_secondary as
 (select ca2.claim_num, min(ca2.create_date) min_ca_date_secondary
    from claim_attorney ca2
   where primary_flag = 'N'
   group by ca2.claim_num)
select c.claim_num,
       c.claim_type_code,
       ad.notification_type_code,
       c.policy_num,
       c.accident_date,
       c.adjuster_code,
       c.claim_unit_code,
       c.claimant_zip,
       c.claimant_dob,
       c.claimant_dod,
       cap.min_ca_date_primary,
       round(cap.min_ca_date_primary - c.accident_date) days_from_accident_to_eoa,
       cas.min_ca_date_secondary,
       ad.notification_type_code,
       a.accident_nature_code,
       a.acc_on_premises_flag,
       a.emerg_room_flag,
       a.hospitalized_flag,
       a.out_patient_flag,
       a.intoxification_flag,
       a.first_report_body_parts,
       a.employer_question_flag,
       a.call_911_flag,
       a.dci_injury_site_zip,
       a.primary_accident_loc,
       a.safety_appl_prov_flag,
       a.safety_appl_used_flag,
       a.surgery_ind,
       a.first_report_atty_flag,
       a.possible_safety_violation_flag,
       a.no_medical_treatment_flag,
       cc.denial_correspond_cnt,
       pc.clm_cnt_on_policy,
       siu_ref.max_siu_referral,
       siu_ref.min_siu_referral,
       round(siu_ref.min_siu_referral - c.accident_date) SIU_days_since_accident,
       mcm.min_date_of_nurse_ref,
       round(mcm.min_date_of_nurse_ref - c.accident_date) MCM_days_since_accident,
       f.first_ind_payment_date,
       round(f.first_ind_payment_date - c.accident_date) IND_PAY_days_since_accident,
       i.injury_code,
       i.injury_sev_code,
       i.injury_side_code,
       ind.manual_class_num,
       ind.industry_group_code,
       s.first_settlement_date,
       s.settlement_sum,
       s.num_of_settlements_on_claim,
       pe.min_pinnacol_eoa_date,
       pe.max_pinnacol_eoa_date,
       
       pe.num_pinnacol_eoa_events
  from claim c
 inner join accident a
    on a.claim_num = c.claim_num
 inner join policy_data pc
    on pc.claim_num = c.claim_num
 inner join injury i
    on i.claim_num = c.claim_num
   and i.primary_injury_flag = 'Y'
 inner join industry_data ind
    on ind.claim_num = c.claim_num
  left join benefits_notification ad
    on ad.claim_num = c.claim_num
   and ad.current_notification_flag = 'Y'
  left join claimant_atty_primary cap
    on cap.claim_num = c.claim_num
  left join claimant_atty_secondary cas
    on cas.claim_num = c.claim_num
  left join denial_correspond cc
    on cc.claim_num = c.claim_num
  left join first_nurse_referral_date mcm
    on mcm.claim_num = c.claim_num
  left join first_indemnity_payment_date f
    on f.claim_num = c.claim_num
  left join settlements s
    on s.claim_num = c.claim_num
  left join pinnacol_eoa pe
    on pe.claim_num = c.claim_num
  left join siu_ref
    on siu_ref.pinnacol_num = c.claim_num
 where EXTRACT(YEAR from c.accident_date) > 2018
--and c.claim_num in(3897162,3544188,3960400)
 order by c.claim_num

/*

EMPLOYMENT TERMINATED QUERY 
I read a few notepads and most references to terminated related to employments
however some related to meds terminated or TTD terminated.  Some notepads will say terminated for cause 
and others just say employer terminated employee.  

select  mc.code, mc.code_descr
      , REPLACE(dbms_lob.substr(cms.memo_text,3000),  ',', ' ' )
      ,dbms_lob.getlength(cms.memo_text)
      ,cms.*
 from 
  claim_memo_summary cms
  ,memo_code mc
where cms.memo_code = mc.code
and EXTRACT(YEAR from cms.memo_date) = 2019
and lower(dbms_lob.substr(cms.memo_text,3000)) like '%terminated%'
*/
