with industry_data as
 (select froi.claim_num,
         mcb.manual_class_num,
         mcb.industry_group_code,
         mcb.manual_class_type_code,
         mcb.manual_class_descr
    from first_report_of_injury froi, manual_class_base mcb
   where froi.manual_class_num = mcb.manual_class_num
     and froi.accident_date between mcb.manual_class_rate_eff_date and
         NVL(mcb.manual_class_rate_exp_date, sysdate + 1)
     and rate_file_seq_num =
         (select min(rate_file_seq_num)
            from manual_class_base m2
           where mcb.manual_class_num = m2.manual_class_num
             and sysdate between m2.manual_class_rate_eff_date and
                 NVL(m2.manual_class_rate_exp_date, sysdate + 1))),
first_nurse_referral_date as
 (select min(action_plan_date) as min_date_of_nurse_ref, r.claim_num
    from rn_action_plan r, claim c
   where r.claim_num = c.claim_num
   group by r.claim_num),
first_indemnity_payment_date as
 (select min(trans_date) as first_ind_payment_date, cft.claim_num
    from claim_financial_trans cft
   group by cft.claim_num),
denial_correspond as
 (select count(*) as denial_correspond_cnt, cc.claim_num
    from claim_correspond cc
   where cc.correspond_code in ('C328', 'C333', 'C334')
   group by cc.claim_num),
siu_ref as
 (select siu.pinnacol_num,
         min(siu.create_date) min_siu_referral,
         max(siu.create_date) max_siu_referral,
         count(*) num_siu_referrals
    from siu_referral siu
   group by siu.pinnacol_num),
claimant_atty_primary as
 (select ca.claim_num, min(ca.create_date) min_ca_date_primary
    from claim_attorney ca
   where primary_flag = 'Y'
    and create_date is not null
   group by ca.claim_num),
claim_gross_incurred_total as
  (select cta.claim_num, cta.gross_incurred_total gross_incurred_total
    from claim_totals_audit cta
    where cta.claim_totals_seq_num = (select max(cta2.claim_totals_seq_num) 
                                        from claim_totals_audit cta2 
                                         where cta2.claim_num = cta.claim_num )),
claim_gross_incurred_total_aty as
  (select cta.claim_num, cta.gross_incurred_total gross_incurred_total_at_eoa
    from claim_totals_audit cta
    where cta.claim_totals_seq_num = (select max(cta2.claim_totals_seq_num) 
                                        from claim_totals_audit cta2 
                                           , claim_attorney ca 
                                         where cta2.claim_num = cta.claim_num
                                         and cta2.claim_num = ca.claim_num
                                         and ca.primary_flag = 'Y'
                                         and ca.create_date is not null
                                         and cta2.claim_totals_audit_date <= ca.create_date)),
                                         
minimum_pinnacol_legal_event as -- checking if already referred to legal
(select ceh.claim_num,
         min(ceh.create_date) min_pinnacol_date,
         count(*) num_pinnacol_events
    from cm_event_hierarchy ceh
  -- where ceh.cm_event_type_id = 35
   group by ceh.claim_num)
-- FINAL FIELD SELECTION --
select c.claim_num,
       c.claim_type_code,
       c.accident_date,
       c.claim_unit_code,
       c.claim_status_code,
       cap.min_ca_date_primary,
       round(cap.min_ca_date_primary - c.accident_date) days_from_accident_to_eoa,
       a.accident_nature_code,
       a.acc_on_premises_flag,
       a.emerg_room_flag,
       a.hospitalized_flag,
       a.out_patient_flag,
       a.call_911_flag,
       a.surgery_ind,
       a.no_medical_treatment_flag,
       round(siu_ref.min_siu_referral - c.accident_date) SIU_days_since_accident,
       round(mcm.min_date_of_nurse_ref - c.accident_date) MCM_days_since_accident,
       round(f.first_ind_payment_date - c.accident_date) IND_PAY_days_since_accident,
       i.injury_code,
       i.injury_sev_code,
       ind.industry_group_code,
       cgit.gross_incurred_total,
       cgita.gross_incurred_total_at_eoa
from claim c
 inner join accident a
    on a.claim_num = c.claim_num
 inner join injury i
    on i.claim_num = c.claim_num
   and i.primary_injury_flag = 'Y'
 inner join industry_data ind
    on ind.claim_num = c.claim_num
  left join claimant_atty_primary cap
    on cap.claim_num = c.claim_num
  left join denial_correspond cc
    on cc.claim_num = c.claim_num
  left join first_nurse_referral_date mcm
    on mcm.claim_num = c.claim_num
  left join first_indemnity_payment_date f
    on f.claim_num = c.claim_num
  left join siu_ref
    on siu_ref.pinnacol_num = c.claim_num
  left join claim_gross_incurred_total cgit
    on cgit.claim_num = c.claim_num 
  left join claim_gross_incurred_total_aty cgita 
    on cgita.claim_num = c.claim_num
  left join minimum_pinnacol_legal_event mple
    on mple.claim_num = c.claim_num
-- FILTER --
 where EXTRACT(YEAR from c.accident_date) >= 2019
    -- remove all open claims that haven't received an eoa (these can skew classification)
    AND (cap.min_ca_date_primary IS NOT NULL OR c.claim_status_code != 'A')
  -- CREATE TRAINING SET --
    AND ABS(REMAINDER(ORA_HASH(c.CLAIM_NUM), 200)) <= 96
 --and c.claim_num in (10058673,10059767)
 order by c.claim_num
