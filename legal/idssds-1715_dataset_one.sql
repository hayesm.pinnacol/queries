/* These queries are based on notes from the document Legal Claims Predictor and
   from the meeting with Grant Butterfield on 2/28/20
   Document locations: https://pinnacol.app.box.com/notes/614959523706

 --Data Set One - predict claim_attorney
1. Admissionss
  denials
  denials pending further investagaions, at n days
2. policyholder s
3. industry
4. claim data

5. IME
6. Prior Auth Denied *334,335 claim_correspondance
7. Injury shoulder, hip, working unit, scheduled (reserve or admission)
8. reduced benefits

*/
 --time interval between DOI and EOP

 select c.claim_num, c.accident_date, ca.entry_of_app_date, (ca.entry_of_app_date - c.accident_date) days_from_accident_to_eoa
 from claim c
    , claim_attorney ca
where c.claim_num = ca.claim_num
and EXTRACT( YEAR from c.accident_date) = 2019

--type of accident (and additional accident date )

select c.claim_num, a.accident_cause_code,acc.code_descr
     , a.accident_nature_code, anc.code_descr, a.acc_on_premises_flag
     , a.emerg_room_flag, a.hospitalized_flag, a.out_patient_flag, a.intoxification_flag
     , a.first_report_body_parts, a.employer_question_flag, a.call_911_flag
     , a.dci_injury_site_zip, a.primary_accident_loc
     , a.safety_appl_prov_flag, a.safety_appl_used_flag, a.surgery_ind, a.first_report_atty_flag
     , a.possible_safety_violation_flag, a.no_medical_treatment_flag
     , a.accident_descr1, a.accident_descr2, a.accident_descr3, a.accident_descr4
from claim c
   inner join accident a on a.claim_num = c.claim_num
   inner join accident_cause_code acc on acc.code = a.accident_cause_code
   inner join accident_nature_code anc on anc.code = a.accident_nature_code
where EXTRACT( YEAR from c.accident_date) = 2019;

--employment status *** Talk to TJ
--I read a few notepads and most references to terminated related to employment
--however some related to meds terminated or TTD terminated.  Some notepads will say terminated for cause
--and others just say employer terminated employee.
select  mc.code, mc.code_descr
      , REPLACE(dbms_lob.substr(cms.memo_text,3000),  ',', ' ' )
      ,dbms_lob.getlength(cms.memo_text)
      ,cms.*
 from
  claim_memo_summary cms
  ,memo_code mc
where cms.memo_code = mc.code
and EXTRACT(YEAR from cms.memo_date) = 2019
and lower(dbms_lob.substr(cms.memo_text,3000)) like '%terminated%'


--involvement of controlled substances  ***

select mc.code_descr, REPLACE(dbms_lob.substr(cms.memo_text,3000),  ',', ' ' )
,dbms_lob.getlength(cms.memo_text),
  cms.*
 from
  claim_memo_summary cms
  ,memo_code mc
where cms.memo_code = mc.code
and EXTRACT(YEAR from cms.memo_date) = 2019
and cms.memo_code = 'NARC'


select mc.code_descr, REPLACE(dbms_lob.substr(cms.memo_text,dbms_lob.getlength(cms.memo_text)),  ',', ' ' ),dbms_lob.getlength(cms.memo_text),
  cms.*
 from
  claim_memo_summary cms
  ,memo_code mc
where cms.memo_code = mc.code
and EXTRACT(YEAR from cms.memo_date) = 2019




select mc.code_descr, REPLACE(dbms_lob.substr(cms.memo_text,3000),  ',', ' ' )
,dbms_lob.getlength(cms.memo_text),
  cms.*
 from
  claim_memo_summary cms
  ,memo_code mc
where cms.memo_code = mc.code
and EXTRACT(YEAR from cms.memo_date) = 2019
and cms.memo_code in ('TFR','CRCD'
                     ,'EI','SSR','PEN','NARC','SCI','DMCI','IMECR','CRF','OPTUM'
                     ,'BOLO','IME','PA','REQ');


--All memo codes
select * from memo_code
order by code_descr;

--All correspondance codes
select * from C
order by code

--prior authorization or denial of pre-authorization
/*
C328	TO PRV: Prior Auth Denied. IME Scheduled
C330	Prior Authorization -Initial Request Approved - (RuleXVI)
C333	Prior Authorization -Initial Request Denied -No Explanation
C334	Prior Authorization -Initial Request Denied -Not Compensable
*/
select c.claim_num,c.claim_type_code, cc.correspond_code,ccode.code_descr
 from claim c
  ,claim_correspond cc
  ,correspond_code ccode
where c.claim_num = cc.claim_num
and cc.correspond_code = ccode.code
and EXTRACT( YEAR from c.accident_date) = 2019
and code in ('C328','C330','C333','C334')
order by c.claim_num

--fatality
select c.claim_num, c.claim_status_code, c.claim_type_code, c.claimant_dod
from claim c
where c.claimant_dod is not null
and  EXTRACT( YEAR from c.accident_date) = 2019

or admission data

select c.claim_num, bn.current_notification_flag, bn.notification_type_code
from benefits_notification bn
    ,benefit_period bp
    ,claim_benefit_notification cbn
    ,claim c
where cbn.benefit_period_seq_num = bp.benefit_period_seq_num
and cbn.benefits_notification_seq_num = bn.benefits_notification_seq_num
and c.claim_num = cbn.claim_num
and EXTRACT (YEAR from c.accident_date) = 2018
and bn.current_notification_flag = 'Y'
and bn.notification_type_code in ('FGA','FFA')
order by 1


--frequency of claims per policy holder
select c.claim_num, p.policy_num, p.policy_name, pp.policy_period_eff_date, c.accident_date, pp.policy_period_exp_date, count(*) OVER (partition by c.claim_num) as clm_cnt
from claim c
    ,policy_period pp
    ,policy p
where c.policy_num = p.policy_num
and p.policy_num = pp.policy_num
and EXTRACT( YEAR from c.accident_date) = 2019
and c.accident_date between pp.policy_period_eff_date and pp.policy_period_exp_date
order by 7 desc

 --industry - need help with query
 --union membership - where are forms see note "identified through forms (e.g. 842,124)
--This query gives the ncci industry code associated with the froi.manual_class_num.  "Quick and dirty" according to experts.  There is a much more convoluted way to get to this.
Notes from Mike J

mike.jacobs  1:47 PM
Hey, our discussion percolated in my brain a bit and I thought of a couple of other considerations...
First, I looked and there typically is no expiration date on manual_class_base, so you really should NVL to the Eff Date + 12 months.
Second, you shouldn't use a between (since it's inclusive of the end date), instead use >= manual_class_rate_eff date and < manual_class_rate_exp_date.
Finally, you should really use the policy_period_eff_date for doing the lookup, rather than the accident_date.  Rating values apply for the duration of a policy period based on its eff_date.  So for a policy eff in November, the rating values apply until the next Nov.  The industry group assignments don't change very often, but you'd get a different answer using the accident date if its after the first of the year.  Make sense?


select froi.claim_num, froi.accident_date,mcb.manual_class_num, mcb.industry_group_code, igc.code_descr industry_group_descr -- mcb.manual_class_num, mcb.hazard_group, mcb.industry_group_code, mcb.manual_class_type_code,mcb.sic_code, mcb.manual_class_descr
from first_report_of_injury froi
   , manual_class_base mcb
   ,industry_group_code igc
where froi.manual_class_num = mcb.manual_class_num
and mcb.industry_group_code = igc.code
--and froi.manual_class_num = 772060
--and froi.claim_num = 3992485
and EXTRACT ( YEAR from froi.accident_date) = 2019
and froi.accident_date between  mcb.manual_class_rate_eff_date and NVL(mcb.manual_class_rate_exp_date,sysdate + 1)
and rate_file_seq_num = (select min(rate_file_seq_num)
                        from  manual_class_base m2
                        where mcb.manual_class_num = m2.manual_class_num
                        and sysdate between  m2.manual_class_rate_eff_date and NVL(m2.manual_class_rate_exp_date,sysdate + 1))

order by claim_num

--length of backlog, metric?
--volume of incoming IW call attempts - where tracked?
--volume of outstanding return calls - where tracked?

--Billing of missed appoints - need more info

--no response C441 is TO EMP: Please contact me Letter.
--C504 is Notice of Claim to all Employers, Employees and Providers
--C505 is Request and Form: Claiming PPD
--Not sure the C504 and C505 are correspond codes in this case
 select c.claim_num,c.claim_type_code, cc.correspond_code,ccode.code_descr
 from claim c
  ,claim_correspond cc
  ,correspond_code ccode
where c.claim_num = cc.claim_num
and cc.correspond_code = ccode.code
and EXTRACT( YEAR from c.accident_date) = 2019
and code in ('C441')
order by c.claim_num

--Percent of previous policyholder claims with attornys
select count(*), policy_num
from claim c
inner join claim_attorney ca on ca.claim_num = c.claim_num
where extract( YEAR from c.accident_date) > 2014
group by policy_num
order by count(*) desc

select count(*), policy_num
from claim c
where extract( YEAR from c.accident_date) > 2014
group by policy_num
order by count(*) desc

--unwillingness to provide wage records - where tracked?
--time interval between policy_cancellation and doi - need to find cancellation data

--legal action initiated by pinnacol need more info.  Does this include subro or only docket?

--This is hearings and EOA pinnacol legal events where the claimant does not have an attorney
select c.claim_num, ceh.create_date ,t.code, t.descr, count(*) over (partition by c.claim_num) as num_of_legal_events
from claim c
inner join  cm_event_hierarchy ceh on c.claim_num = ceh.claim_num
inner join  cm_event_type t on ceh.cm_event_type_id = t.id and t.id in (1,35)
--inner join claim_attorney ca on ca.claim_num = c.claim_num
where extract( YEAR from c.accident_date) = 2019
and c.claim_num not in (select claim_num from claim_attorney ca)
order by claim_num, create_date

--time interval between SIU and DOI


select c.claim_num, c.accident_date, sr.create_date, round(sr.create_date - c.accident_date) SIU_days_since_accident
from claim c
inner join siu_referral sr on sr.pinnacol_num = c.claim_num
where extract( YEAR from c.accident_date) = 2019
order by 4


--type of injury (look for hips and shoulders )


--time interval between med only goes to loss time claim & DOI
--change of physician
--IW geographical location
--IW distance to MD
--is healter provider concentra?
--length of time under investigation
--admission denials
select *
from benefits_notification bn
where notification_type_code in ('D1')
and EXTRACT (YEAR from bn.create_date) = 2019

select * from notification_type_code where code in ('D1','D1B');
--note:  there has not be a D1B since 10/3/2005 12:42:15 PM
