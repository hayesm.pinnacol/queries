
/*
RM_REQUESTS_VIEW - this view contains data for the three views below it but it is very slow.  I don't recommend using it.
  rm_group_request_view
  rm_request_info_view
  RM_REQUESTS_VIEW
*/

--Policy Request type
select  dbms_lob.substr(VISIT_DESCR,500),
     dbms_lob.getlength(VISIT_DESCR),
     lv.*
 from  RM_REQUESTS_VIEW lv
where  extract(YEAR from create_date) > 2019
--Group requests
select  dbms_lob.substr(VISIT_DESCR,500),
     dbms_lob.getlength(VISIT_DESCR),
     gv.*
 from  RM_GROUP_REQUEST_VIEW gv
where  extract(YEAR from create_date) > 2019

--Loss Prevention requests
select  dbms_lob.substr(VISIT_DESCR,500),
     dbms_lob.getlength(VISIT_DESCR),
     lpv.*
 from  RM_LPPROJ_REQUEST_VIEW lpv
where  extract(YEAR from create_date) > 2019
/
--claim requests
select * from rm_request_claim_view
where  extract(YEAR from create_date) > 2019 ;
/
--contacts.
select * from rm_request_contact_view
where  extract(YEAR from create_date) > 2019 ;
