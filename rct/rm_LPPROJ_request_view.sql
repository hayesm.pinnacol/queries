CREATE OR REPLACE FORCE EDITIONABLE VIEW "WCIS_DBA"."RM_LPPROJ_REQUEST_VIEW" ("REQUEST_SEQ_NUM", "ASSOCIATION_CODE", "REQUEST_ASSOCIATION_NAME", "GROUP_ASSOCIATION_SEQ_NUM", "PROJECT_SEQ_NUM", "POLICY_NUM", "BUS_SEQ_NUM", "LOC_SEQ_NUM", "REQUESTOR_USER_NAME", "ASSIGNED_TO_USER_NAME", "REQUEST_TYPE_CODE", "REQUEST_TYPE", "ON_LAPTOP", "REFERRAL_TYPE_CODE", "INSTRUCTIONS", "VISIT_DESCR", "AGENCY_CONTACT_REQUIRED", "PRIMARY_POLICY_CONTACT", "PRIMARY_AGENCY_CONTACT", "PRIMARY_GROUP_CONTACT", "SAFETY_CONSULTANT_NOTES", "CURRENT_MPS", "DUE_DATE", "VISIT_DATE", "COMPLETION_DATE", "VISIT_TIME", "ADMIN_TIME", "TRAVEL_TIME", "OFFICE_MGMT_REVIEW", "JOBSITE_REVIEW", "FIXED_FACILITY_REVIEW", "OTHER_CLIENT_REVIEW", "PHONE_REVIEW", "LETTER_REVIEW", "LAPTOP_STATUS_CODE", "CREATE_DATE", "CREATE_USER", "UPDATE_DATE", "UPDATE_USER", "BUS_NAME", "LOCATION_NAME", "LOCATION_NAME2", "LOC_ADDR1", "LOC_ADDR2", "LOC_CITY", "LOC_PHONE", "LOC_STATE", "LOC_ZIP", "LOC_ZIP4", "LOC_EMAIL_ADDRESS", "POLICY_NAME", "POLICY_PERIOD_EFF_DATE", "POLICY_PERIOD_EXP_DATE", "POLICY_UNDERWRITER_USER_NAME", "POLICY_CLAIM_UNIT_CODE", "POLICY_LOSS_PREV_REP_USER_NAME", "LPREP_NAME", "LPREP_PHONE_EXT", "UW_NAME", "UW_PHONE_EXT", "REQUESTOR_NAME", "REQUESTOR_PHONE_EXT", "AGENT_NAME", "AGENT_NUMBER", "POLICY_ASSOCIATION_CODE", "POLICY_ASSOCIATION_NAME", "THREAT_FLAG") AS 
  select r.request_seq_num                                        request_seq_num,
       null                                                     association_code,
       null                                                     request_association_name,       -- ASSOCIATION_NAME
       to_number(null)                                          group_association_seq_num,
       r.project_seq_num                                        project_seq_num,
       r.policy_num                                             policy_num,
       r.bus_seq_num                                            bus_seq_num,
       r.loc_seq_num                                            loc_seq_num,
       r.requestor_user_name                                    requestor_user_name,
       r.assigned_to_user_name                                  assigned_to_user_name,
       r.request_type_code                                      request_type_code,
       r.request_type                                           request_type,
       r.on_laptop_flag                                         on_laptop,
       r.referral_type_code                                     referral_type_code,
       r.instructions                                           instructions,
       r.visit_descr                                            visit_descr,
       r.agency_contact_required_flag                           agency_contact_required,
       r.primary_policy_contact                                 primary_policy_contact,
       r.primary_agency_contact                                 primary_agency_contact,
       r.primary_group_contact                                  primary_group_contact,
       r.safety_consultant_notes                                safety_consultant_notes,
       r.current_mps                                            current_mps,
       r.due_date                                               due_date,
       r.visit_date                                             visit_date,
       r.completion_date                                        completion_date,
       r.visit_time                                             visit_time,
       r.admin_time                                             admin_time,
       r.travel_time                                            travel_time,
       r.office_mgmt_review_flag                                office_mgmt_review,
       r.jobsite_review_flag                                    jobsite_review,
       r.fixed_facility_review_flag                             fixed_facility_review,
       r.other_client_review_flag                               other_client_review,
       r.phone_review_flag                                      phone_review,
       r.letter_review_flag                                     letter_review,
       r.laptop_status_code                                     laptop_status_code,
       r.create_date                                            create_date,
       r.create_user                                            create_user,
       r.update_date                                            update_date,
       r.update_user                                            update_user,
       lp.project_name                                          bus_name,
       lp.project_name                                          location_name,
       null                                                     location_name2,
       null                                                     loc_addr1,
       null                                                     loc_addr2,
       null                                                     loc_city,
       null                                                     loc_phone,
       null                                                     loc_state,
       null                                                     loc_zip,
       null                                                     loc_zip4,
       null                                                     loc_email_address,               -- email address
       lp.project_name                                          policy_name,
       wcisdate(sysdate)                                        policy_period_eff_date,          -- PP.POLICY_PERIOD_EFF_DATE
       wcisdate(sysdate)                                        policy_period_exp_date,          -- PP.POLICY_PERIOD_EXP_DATE
       null                                                     policy_underwriter_user_name,    -- PP.UNDERWRITER_USER_NAME
       null                                                     policy_claim_unit_code,          -- PP.CLAIM_UNIT_CODE
       null                                                     policy_loss_prev_rep_user_name,  -- PP.LOSS_PREV_REP_USER_NAME
       lpr.employee_first_name || ' ' || lpr.employee_last_name lprep_name,
       lpr.phone_ext                                            lprep_phone_ext,
       null                                                     uw_name,   -- UW.EMPLOYEE_FIRST_NAME || ' ' || UW.EMPLOYEE_LAST_NAME
       null                                                     uw_phone_ext,
       req.employee_first_name || ' ' || req.employee_last_name requestor_name,
       req.phone_ext                                            requestor_phone_ext,
       null                                                     agent_name,
       to_number(null)                                          agent_number,
       null                                                     policy_association_code,
       null                                                     policy_association_name,
       null                                                     threat_flag
  from rm_request r, wcis_employee lpr, wcis_employee req, rm_lp_project lp
 where lp.project_seq_num = r.project_seq_num
   and r.request_type = 'LPPROJ'
   and lpr.user_name (+) = r.assigned_to_user_name
   and req.user_name (+) = r.requestor_user_name;
  