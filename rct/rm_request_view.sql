CREATE OR REPLACE FORCE EDITIONABLE VIEW "WCIS_DBA"."RM_REQUEST_INFO_VIEW" ("REQUEST_SEQ_NUM", "ASSOCIATION_CODE", "REQUEST_ASSOCIATION_NAME", "GROUP_ASSOCIATION_SEQ_NUM", "PROJECT_SEQ_NUM", "POLICY_NUM", "BUS_SEQ_NUM", "LOC_SEQ_NUM", "REQUESTOR_USER_NAME", "ASSIGNED_TO_USER_NAME", "REQUEST_TYPE_CODE", "REQUEST_TYPE", "ON_LAPTOP", "REFERRAL_TYPE_CODE", "INSTRUCTIONS", "VISIT_DESCR", "AGENCY_CONTACT_REQUIRED", "PRIMARY_POLICY_CONTACT", "PRIMARY_AGENCY_CONTACT", "PRIMARY_GROUP_CONTACT", "SAFETY_CONSULTANT_NOTES", "CURRENT_MPS", "DUE_DATE", "VISIT_DATE", "COMPLETION_DATE", "VISIT_TIME", "ADMIN_TIME", "TRAVEL_TIME", "OFFICE_MGMT_REVIEW", "JOBSITE_REVIEW", "FIXED_FACILITY_REVIEW", "OTHER_CLIENT_REVIEW", "PHONE_REVIEW", "LETTER_REVIEW", "LAPTOP_STATUS_CODE", "CREATE_DATE", "CREATE_USER", "UPDATE_DATE", "UPDATE_USER", "BUS_NAME", "LOCATION_NAME", "LOCATION_NAME2", "LOC_ADDR1", "LOC_ADDR2", "LOC_CITY", "LOC_PHONE", "LOC_STATE", "LOC_ZIP", "LOC_ZIP4", "LOC_EMAIL_ADDRESS", "POLICY_NAME", "POLICY_PERIOD_EFF_DATE", "POLICY_PERIOD_EXP_DATE", "POLICY_UNDERWRITER_USER_NAME", "POLICY_CLAIM_UNIT_CODE", "POLICY_LOSS_PREV_REP_USER_NAME", "LPREP_NAME", "LPREP_PHONE_EXT", "UW_NAME", "UW_PHONE_EXT", "REQUESTOR_NAME", "REQUESTOR_PHONE_EXT", "AGENT_NAME", "AGENT_NUMBER", "POLICY_ASSOCIATION_CODE", "POLICY_ASSOCIATION_NAME", "THREAT_FLAG") AS 
  select r.request_seq_num                                                          request_seq_num,
       r.association_code                                                         association_code,
       gac2.code_descr                                                            request_association_name,
       gac2.group_association_seq_num,
       r.project_seq_num                                                          project_seq_num,
       r.policy_num                                                               policy_num,
       r.bus_seq_num                                                              bus_seq_num,
       r.loc_seq_num                                                              loc_seq_num,
       r.requestor_user_name                                                      requestor_user_name,
       r.assigned_to_user_name                                                    assigned_to_user_name,
       r.request_type_code                                                        request_type_code,
       r.request_type                                                             request_type,
       r.on_laptop_flag                                                           on_laptop,
       r.referral_type_code                                                       referral_type_code,
       r.instructions                                                             instructions,
       r.visit_descr                                                              visit_descr,
       r.agency_contact_required_flag                                             agency_contact_required,
       r.primary_policy_contact                                                   primary_policy_contact, -- Removed decode from primary_policy_contact.  It was causing a locking problem in RM300S.
       r.primary_agency_contact                                                   primary_agency_contact,
       r.primary_group_contact                                                    primary_group_contact,
       r.safety_consultant_notes                                                  safety_consultant_notes,
       r.current_mps                                                              current_mps,
       r.due_date                                                                 due_date,
       r.visit_date                                                               visit_date,
       r.completion_date                                                          completion_date,
       r.visit_time                                                               visit_time,
       r.admin_time                                                               admin_time,
       r.travel_time                                                              travel_time,
       r.office_mgmt_review_flag                                                  office_mgmt_review,
       r.jobsite_review_flag                                                      jobsite_review,
       r.fixed_facility_review_flag                                               fixed_facility_review,
       r.other_client_review_flag                                                 other_client_review,
       r.phone_review_flag                                                        phone_review,
       r.letter_review_flag                                                       letter_review,
       r.laptop_status_code                                                       laptop_status_code,
       r.create_date                                                              create_date,
       r.create_user                                                              create_user,
       r.update_date                                                              update_date,
       r.update_user                                                              update_user,
       b.bus_name                                                                 bus_name,
       l.location_name                                                            location_name,
       l.location_name2                                                           location_name2,
       l.loc_addr1                                                                loc_addr1,
       l.loc_addr2                                                                loc_addr2,
       l.loc_city                                                                 loc_city,
       l.loc_phone                                                                loc_phone,
       l.loc_state                                                                loc_state,
       l.loc_zip                                                                  loc_zip,
       l.loc_zip4                                                                 loc_zip4,
       l.email_address                                                            loc_email_address,
       p.policy_name                                                              policy_name,
       pp.policy_period_eff_date                                                  policy_period_eff_date,
       pp.policy_period_exp_date                                                  policy_period_exp_date,
       pp.underwriter_user_name                                                   policy_underwriter_user_name,
       pp.claim_unit_code                                                         policy_claim_unit_code,
       pp.loss_prev_rep_user_name                                                 policy_loss_prev_rep_user_name,
       lpr.employee_first_name || ' ' || lpr.employee_last_name                   lprep_name,
       lpr.phone_ext                                                              lprep_phone_ext,
       uw.employee_first_name || ' ' || uw.employee_last_name                     uw_name,
       uw.phone_ext                                                               uw_phone_ext,
       decode(req.employee_first_name, '', r.create_user, req.employee_first_name || ' ' ||
                                                          req.employee_last_name) requestor_name,
       req.phone_ext                                                              requestor_phone_ext,
       ( SELECT ag.agent_name
         FROM agent ag
             ,agent_policy_period ap
         WHERE ap.policy_num = pp.policy_num
           AND ap.policy_period_eff_date = pp.policy_period_eff_date
           AND NVL(ap.history_record_flag,'N') <> 'Y'
           AND ap.agent_eff_date <= GREATEST(trunc(r.create_date)
                                            ,ap.policy_period_eff_date )
           AND NVL(ap.agent_exp_date, TRUNC(wcisdate(SYSDATE)))
					    >= LEAST(pp.policy_period_exp_date, trunc(r.create_date))
           and ag.agent_num (+) = ap.agent_num )                                  agent_name,
       ( SELECT ag.agent_num
         FROM agent ag
             ,agent_policy_period ap
         WHERE ap.policy_num = pp.policy_num
           AND ap.policy_period_eff_date = pp.policy_period_eff_date
           AND NVL(ap.history_record_flag,'N') <> 'Y'
           AND ap.agent_eff_date <= GREATEST(trunc(r.create_date)
                                            ,ap.policy_period_eff_date)
           AND NVL(ap.agent_exp_date, TRUNC(wcisdate(SYSDATE)))
					    >= LEAST(pp.policy_period_exp_date, trunc(r.create_date) )
           and ag.agent_num (+) = ap.agent_num )                                  agent_number,
       gac.code                                                                   policy_association_code,
       gac.code_descr                                                             policy_association_name,
       decode(sp90540_threat_exists(p.policy_num, 'POL'), 'Y', '!', null)         threat_flag
  from rm_request r
	    ,wcis_employee lpr
			,wcis_employee uw
			,wcis_employee req
			,business b
			,group_association_code gac
			,group_association_code gac2
			,location l
			,policy p
			,policy_period pp
 where p.policy_num = r.policy_num
   and r.request_type = 'POLICY'
   and pp.policy_num (+) = r.policy_num
   and pp.policy_period_eff_date (+) = r.policy_period_eff_date
   and gac.code (+) = pp.group_assoc_code
   and gac2.code (+) = r.association_code
   and b.policy_num (+) = r.policy_num
   and b.bus_seq_num (+) = r.bus_seq_num
   and l.loc_seq_num (+) = r.loc_seq_num
   and lpr.user_name (+) = r.assigned_to_user_name
   and uw.user_name (+) = pp.underwriter_user_name
   and req.user_name (+) = r.requestor_user_name;