import os
import pickle
import pandas as pd

from dotenv import load_dotenv
from sqlalchemy import create_engine

load_dotenv()

oracle_connection_string = 'oracle+cx_oracle://{username}:{password}@{hostname}:{port}/{database}'
engine = create_engine(
    oracle_connection_string.format(
        username=os.getenv('SWCIS_DB_USER'),
        password=os.getenv('SWCIS_DB_PASSWORD'),
        hostname=os.getenv('SWCIS_DB_HOST'),
        port=os.getenv('SWCIS_DB_HOST_PORT'),
        database=os.getenv('SWCIS_DB_DATABASE'),
    )
)

number_of_years_to_consider = 3
minimum_date_with_full_history = "'01-jan-2015'"
claims_minimum_date = "'01-jan-2012'"

query = f"""
    select c.policy_num,
           c.policy_period_eff_date,
           c.claim_num,
           case when c.claim_status_code = 'Z' then 'CLOSED' else 'ACTIVE' end claim_status,
           c.accident_date,
           nvl(c.c1_recv_date, c.c7_recv_date) reported_date,
           case when nvl(a.safety_appl_prov_flag,'N') = 'Y' then 1 else 0 end safety_appl_prov_flag,
           case when nvl(a.safety_appl_used_flag,'N') = 'Y' then 1 else 0 end safety_appl_used_flag,
           case when nvl(a.possible_safety_violation_flag, 'N') = 'Y' then 1 else 0 end possible_safety_violation_flag,
           a.accident_cause_code,
           c.closed_date,
           ct.gross_incurred_med,
           ct.gross_incurred_ind,
           ct.gross_incurred_exp,
           (select rtrim(rtrim(xmlagg(xmlelement(e, i.injury_code || ', ')).extract('//text()'),','),', ')
              from injury i
             where i.claim_num = c.claim_num
               and i.primary_injury_flag = 'Y') primary_injury,
           (select rtrim(rtrim(xmlagg(xmlelement(e, i.injury_code || ', ')).extract('//text()'),','),', ')
              from injury i
             where i.claim_num = c.claim_num) injuries
      from claim c,
           accident a,
           claim_totals ct
     where a.claim_num = c.claim_num
       and ct.claim_num = c.claim_num
       and c.claim_status_code in ('A','Z')
       and c.policy_period_eff_date >= {claims_minimum_date}
"""

df = pd.read_sql(query, engine)

with open('claims_history.pkl', 'wb') as f:
    pickle.dump(df, f, protocol=pickle.HIGHEST_PROTOCOL)
