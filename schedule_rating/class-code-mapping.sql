-- run against staging
SELECT
    ncci_class_id as ncci_class_code,
    industry_group_id as industry_group_code,
    hazard_group_id as hazard_group
    industry_group_descr,
    ncci_class_descr

FROM D_CLASS

WHERE CURRENT_RECORD = 1
