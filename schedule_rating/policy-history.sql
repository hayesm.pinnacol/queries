-- Run against SWCIS
select x.*,
       case when instr(x.addr_last_10,'-') > 0 then substr(x.addr_last_10,1,5) else substr(x.addr_last_10,-5) end bus_zip,
       (select z.colo_urban_rural
          from zip_county_xref z
         where z.zip = case when instr(x.addr_last_10,'-') > 0 then substr(x.addr_last_10,1,5) else substr(x.addr_last_10,-5) end) urban_rural,
       pk_tpt.get_governing_class(x.policy_num, x.policy_period_eff_date, x.max_phs_id) gov_class,
       (select sum(nvl(phcc.manual_class_period_payroll,0))
          from prem_hist_class_coverage phcc
         where phcc.prem_hist_set_id = x.max_phs_id
           and phcc.policy_num = x.policy_num
           and phcc.policy_period_eff_date = x.policy_period_eff_date) total_payroll,
       pk75000_policy_pkg.payment_level_amt(x.policy_num, x.policy_period_eff_date, 'SRF', 'BILLED', 'BASE', x.max_phs_id) srf_base,
       pk75000_policy_pkg.payment_level_amt(x.policy_num, x.policy_period_eff_date, 'SRF', 'BILLED', 'DIFF', x.max_phs_id) srf_diff,
       sign(pk75000_policy_pkg.payment_level_amt(x.policy_num, x.policy_period_eff_date, 'CCD', 'BILLED', 'BASE', x.max_phs_id)) cost_containment,
       pk75000_policy_pkg.payment_level_amt(x.policy_num,x.policy_period_eff_date,'NEAP', 'BILLED', 'BASE', x.max_phs_id) neap,
       pk75000_policy_pkg.payment_level_amt(x.policy_num,x.policy_period_eff_date,'MAN', 'BILLED', 'DIFF', x.max_phs_id) man,
       pk_loss_ratio.gross(x.policy_num,x.policy_period_eff_date,'CUSTOM',pk_loss_ratio.NO_CAP,sysdate,add_months(x.policy_period_exp_date,-36),x.policy_period_exp_date) gross_36
  from (
select pp.policy_num,
       pp.policy_period_eff_date,
       pp.policy_period_exp_date,
       p.policy_orig_eff_date,
       (select nvl(sum(decode(decode(notice_of_intent_date, null, 'N', nvl(strike_flag, 'Y')), 'Y', 1, 0)), 0)
          from cancellation c, reinstate_reason_code rrc
         where rrc.code(+) = c.reinstate_reason_code
           and c.create_date >= pp.policy_period_eff_date
           and c.create_date < pp.policy_period_exp_date
           and c.policy_num = pp.policy_num) d1_count,
       nvl((select sum(1)
          from audit_schedule a,
               audit_code ac
         where a.policy_period_eff_date = pp.policy_period_eff_date
           and a.policy_num = pp.policy_num
           and a.audit_status_code = 'B'
           and ac.code = a.audit_code
           and ac.inadmissible_div_calc_flag = 'Y'),0) non_compliant_audit,
       substr(pk71470_policy_address_pkg.get_min_bus_min_loc_qry(p.policy_num),-10) addr_last_10,
       pk75000_policy_pkg.max_prem_hist_set(pp.policy_num,pp.policy_period_eff_date) max_phs_id,
       case when pp.policy_period_eff_date = p.policy_orig_eff_date then 1 else 0 end first_period_flag,
       case when pp.policy_period_eff_date = p.policy_orig_eff_date then 0 else 1 end renewal_flag,
       pk75000_policy_pkg.pft_total(pp.policy_num,pp.policy_period_eff_date,'ADP') audit_amt
  from policy_period pp,
       policy p
 where p.policy_num = pp.policy_num
   and pp.policy_period_status_code in ('0','E','FA')
   and pp.policy_period_eff_date >= {minimum_date_with_full_history}
) x
where x.max_phs_id is not null
