/*
 
  1. Premises  Premises section_num = 1
  2. Classification Peculiarities section_num = 5 
  3. Medical Facilities section_num = 10
  4. Safety Devices section_num = 15
  5. Employees - Selection, Training, Supervision section_num = 20  
  6. Management - Cooperation with Insurance Carrier   section_num = 25
  7. Safety Devices section_num = 30
  8. Management - Safety Organization ??
  
3053742.0000000000  11/1/2019  1.0000000000  Premises                                       0.0000000000
3053742.0000000000  11/1/2019  5.0000000000  Classification Peculiarities                   0.1000000000
3053742.0000000000  11/1/2019  10.0000000000	Medical Facilities	                          0.0000000000
3053742.0000000000	11/1/2019	15.0000000000	Safety Devices	                                0.0000000000
3053742.0000000000	11/1/2019	20.0000000000	Employees - Selection, Training, Supervision	  0.1000000000
3053742.0000000000	11/1/2019	25.0000000000	Management - Cooperation with Insurance Carrier	0.0000000000
3053742.0000000000	11/1/2019	30.0000000000	Management - Safety Organization	              0.0500000000

underwriter_user_name  HANSENC
policy_num  3053742
policy_period_eff_date  11/1/2019
schedule_rating_adjustment_uw  0.75
class_peculiarities_contribution_uw  -0.1
employee_selection_contribution_uw  -0.1
management_cooperation_contribution_uw  0
management_safety_contribution_uw  -0.05
medical_facilities_contribution_uw  0
premises_contribution_uw  0
safety_devices_contribution_uw  0
designated_provider_discount_contribution_uw  0.025

*/
select p.policy_num
   --   ,p.policy_name
   --   ,p.admin_sbu_team_code
   --   ,pw.create_user
   --   ,pw.update_user
     ,pw.policy_period_eff_date
   --  ,pw.policy_worksheet_seq
   --  ,pw.worksheet_status_code
  --   ,pw.worksheet_num
 --    ,pw.comment_text
   --  ,pws.section_num
     ,v.section_num
     ,v.section_text
 --    ,pws.comment_text
     ,pwsc.value questions_score
 --    ,v.section_score_min
 --    ,v.section_score_min
 
from policy p
    ,policy_worksheet pw
    ,policy_worksheet_section pws
    ,policy_worksheet_score pwsc
    ,policy_worksheet_sec_score_vw v
where p.policy_num = pw.policy_num
and pw.policy_worksheet_seq = pws.policy_worksheet_seq
and pw.worksheet_num = pws.worksheet_num
and pws.policy_worksheet_seq = pwsc.policy_worksheet_seq
and pws.section_num = pwsc.section_num
and v.policy_worksheet_seq = pw.policy_worksheet_seq
and v.worksheet_num = pw.worksheet_num
and v.section_num = pws.section_num
and pw.policy_worksheet_seq = (select max(policy_worksheet_seq) from policy_worksheet pw2
                                where pw2.policy_num = pw.policy_num
                                and extract(YEAR from pw2.policy_period_eff_date) = 2019)
and p.policy_num = 3053742
and extract(YEAR from pw.policy_period_eff_date) = 2019
order by v.section_num
                                
