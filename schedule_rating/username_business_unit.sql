/* 
underwriter_user_name	
business_unit_code	
business_unit_desc

To get underwriters only, uncomment  and employee_class_code = 11  --11 is underwriter 
To get only active employees, uncomment and employee_status_code = 'PA' --PA is active 
*/

select user_name, sbu_team_code , ou.code, ou.code_descr, e.employee_class_code_descr
 from wcis_employee we
     ,employee_class_code e
     ,org_unit ou 
where ou.code = we.sbu_team_code 
and e.employee_class_code = we.employee_class_code
and sbu_team_code in ('DEST','OSI')
--and employee_status_code = 'PA' --PA is active 
--and employee_class_code = 11  --11 is underwriter 
/
