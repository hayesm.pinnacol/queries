-- run against SWCIS
select pp.underwriter_user_name,
       pp.policy_num, pp.policy_period_eff_date,
       (select max(phpa.prem_adjustment) from prem_hist_prem_adjustment phpa
         where phpa.policy_num = pp.policy_num
           and phpa.policy_period_eff_date = pp.policy_period_eff_date
           and phpa.prem_adjustment_type_code = 'SRF'
           and phpa.reference_seq_num = pwss.policy_worksheet_seq ) srf_adjustment,
       sp67500_policy_worksheet_disc(pwss.policy_worksheet_seq) discount_pct,
       pwss.*
  from policy_worksheet_sec_score_vw pwss,
       policy_worksheet pw,
       policy_period pp
 where pwss.policy_worksheet_seq = pw.policy_worksheet_seq
   and pp.policy_num = pw.policy_num
   and pp.policy_period_eff_date = pw.policy_period_eff_date
   and pp.policy_period_eff_date > '01-jan-2019'
   and pw.policy_worksheet_seq = (
        select max(x.policy_worksheet_seq)
        from policy_worksheet x
        where x.policy_num = pw.policy_num
        and x.policy_period_eff_date = pw.policy_period_eff_date
       )

order by
    pp.underwriter_user_name,
    policy_period_eff_date,
    policy_num,
    section_num
