-- run against staging
SELECT
    injury_cause_key,
    injury_cause_code,
    injury_cause,
    injury_cause_group,
    standard_rpts_group_code

FROM INJURY_CAUSE_DIM
