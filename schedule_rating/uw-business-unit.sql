-- run against SWCIS
SELECT
  user_name as underwriter_user_name,
  SBU_TEAM_CODE as business_unit_code,
  ou.CODE_DESCR as business_unit_desc

FROM WCIS_EMPLOYEE we

JOIN ORG_UNIT ou
ON ou.CODE = we.SBU_TEAM_CODE
