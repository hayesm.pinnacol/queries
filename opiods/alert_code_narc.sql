select /*+ rule */
     --  level             lev,
     --  h.id,
     --  h.mm_category_id,
     --  h.parent_id,
     --  h.hierarchy_id,
       c.descr,
       c.mm_category_type_code,
       pc.procedure_type_code,
       pc.procedure_prefix_code,
       pc.procedure_code
  from mm_hierarchy h
     ,mm_procedure_code pc
     ,mm_category c
where pc.mm_hierarchy_id = h.id
and h.mm_category_id = c.id--
--and pc.procedure_code =  '00406012301'
connect by prior h.id = h.parent_id
start with parent_id = 2736--0;
