
--  very slow 
select /*+ rule */
       level             lev,
       h.id,
       h.mm_category_id,
       h.parent_id,
       h.hierarchy_id,
       mbs.claim_num,
       mbs.bill_seq_num,
       ppc.*,
       c.*,
       pc.*
  from mm_hierarchy h
     ,mm_procedure_code pc
     ,mm_category c
     ,procedure_code ppc
     ,med_bill_summary mbs
     ,med_bill_detail mbd
where pc.mm_hierarchy_id = h.id
and h.mm_category_id = c.id
and mbs.version_num = mbd.version_num
and mbs.bill_seq_num = mbd.bill_seq_num
and mbd.allowed_procedure_code_seq_num = ppc.procedure_code_seq_num
and pc.thru_date is null
and pc.procedure_code = ppc.procedure_code
and pc.procedure_prefix_code = ppc.procedure_prefix_code
and pc.procedure_type_code = ppc.procedure_type_code
--and ppc.thru_date is null
--and pc.procedure_code =  '00406012301'
and mbs.claim_num = 3945850
connect by prior h.id = h.parent_id
start with parent_id = 2736--0;
