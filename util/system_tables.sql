--meta data about wcis tables

--all_dependencies will show dependencies for a table
select * from all_dependencies ad
where ad.REFERENCED_NAME = 'RM_REQUEST'
order by type
/

--All_tables helps find a table related to keyword or if you can not remember the table name.
--note:  Always check the max create_date before using a table to make sure it is currently being used.
select * from all_tables at
where at.table_name like 'RM%'
and at.owner = 'WCIS_DBA'
/
--all_tab_columns helps find table columns
select * from all_tab_columns atc
where atc.owner = 'WCIS_DBA'
and column_name like '%REQUEST%'
/
--extracting clob data
select  mc.code, mc.code_descr
      , REPLACE(dbms_lob.substr(cms.memo_text,3000),  ',', ' ' )
      ,dbms_lob.getlength(cms.memo_text)
      ,cms.*
 from
  claim_memo_summary cms
  ,memo_code mc
where cms.memo_code = mc.code
and EXTRACT(YEAR from cms.memo_date) = 2019
and lower(dbms_lob.substr(cms.memo_text,3000)) like '%terminated%'
/
