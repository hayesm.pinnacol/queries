/*
  This query returns claims that have been assigned to a nurse.  Because there can be multiple referrals to a nurse, we are looking 
  for the earliest date that a nurse referral happened. Also we are only looking for specific nurse types because every claim is assigned 
  a nurse, but normally it is the claims rep.  
  
  CATRN	Catastrophic Nurse
  DRRN	DELAYED RECOVERY NURSE  
  EIRN	EARLY INTERVENTION NURSE
  TRGRN	Triage Nurse Case Manager
  URRN	UR Nurse

  This is also returning the total bill amount for the current version of each bill that was greater that 5000 and occurred before the nurse referral.
  The medical bill line details are included to get the service_begin_date.  The medical bill lines are limited to
  the first line item because the service_date is the same for each detail. The claim attorney is left joined to indicate if the
  claim is represented.  The ANSI style join is used to allow the left join to be qualified to only return primary attorneys.  
  Claim_num, accident_date, min_date_of_nurse_ref, attorney_num and primary_flag are redundent because of the inclusion of medical bill data.
  If there are more than one bill > 5000 prior to the nurse referral there will be a row for each unique bill_seq_num on the current version of the bill. 
     
  
*/
with first_nurse_audit_row as (
    select min(entry_date) as min_date_of_nurse_ref, claim_num 
    from claim_audit ca 
        ,adjuster a 
    where ca.new_value = a.adjuster_code
    and a.adjuster_type_code in  ('URRN','CATRN','TRGRN','EIRN','DRRN') 
    and claim_audit_type_code = 'NUR'  
    group by claim_num 
)
select c.claim_num , c.accident_date
     , n.min_date_of_nurse_ref
     , ca.attorney_num , ca.primary_flag
     , b.bill_seq_num , b.version_num  , b.total_billed_amt , d.service_begin_date
from claim c
     inner join first_nurse_audit_row n on n.claim_num = c.claim_num
     inner join med_bill_summary b on b.claim_num = c.claim_num and b.current_flag = 'Y' 
     inner join med_bill_detail d on d.bill_seq_num = b.bill_seq_num and d.version_num = b.version_num and d.bill_line_num = 1
     left join claim_attorney ca on ca.claim_num = c.claim_num and ca.primary_flag = 'Y'
  --where c.claim_num in (3966447, 3959900)
  where extract( year from c.accident_date) > 2009
  and b.total_billed_amt > =5000
  and b.enter_date <= n.min_date_of_nurse_ref
  order by c.accident_date
