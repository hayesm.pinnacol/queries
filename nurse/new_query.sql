--my version of query 
with all_icd as
 (select mbs.claim_num claim_num,
         listagg(NVL(mbs.ICD9_CODE1, '') || ' ' || NVL(mbs.ICD9_CODE2, '') || ' ' ||
                 NVL(mbs.ICD9_CODE3, '') || ' ' || NVL(mbs.ICD9_CODE4, ''),
                 ', ') WITHIN GROUP(ORDER BY mbs.bill_seq_num) as ALL_ICD_CODES
    from med_bill_summary mbs, claim c
   where mbs.claim_num = c.claim_num
     and mbs.current_flag = 'Y'
     and mbs.enter_date <= c.accident_date + 30
   group by mbs.claim_num),
all_billing_codes as
 (select mbs.claim_num claim_num,
         listagg(mbd.billed_procedure_prefix_code ||
                 mbd.billed_procedure_code || ',') WITHIN GROUP(ORDER BY mbd.detail_seq_num) as ALL_BILLED_PROC_CODES
    from med_bill_summary mbs, med_bill_detail mbd, claim c
   where mbs.bill_seq_num = mbd.bill_seq_num
     and mbs.version_num = mbd.version_num
     and mbs.claim_num = c.claim_num
     and mbs.enter_date <= c.accident_date + 30
     and mbs.current_flag = 'Y'
   group by mbs.claim_num),
first_nurse_audit_row as
 (select min(action_plan_date) as min_date_of_nurse_ref, r.claim_num
    from rn_action_plan r, claim c
   where r.claim_num = c.claim_num
   group by r.claim_num),
first_indemnity_payment_date as
 (select min(trans_date) as first_ind_payment_date, cft.claim_num
    from claim_financial_trans cft
   group by cft.claim_num),
siu_ref as
 (select siu.pinnacol_num,
         min(siu.create_date) min_siu_referral,
         max(siu.create_date) max_siu_referral,
         count(*) num_siu_referrals
    from siu_referral siu
   group by siu.pinnacol_num)
select c.claim_num,
       c.accident_date,
       c.claim_type_code,
       c.claimant_sex,
       c.claimant_dob,
       c.claimant_language_code,
       c.claimant_language_other,
       c.nlt_to_lt_date,
       c.nlt_to_lt_reason_code,
       c.black_lung_flag,
       c.deny_liability_flag,
       c.severity_code,
       c.garnishment_flag,
       c.workers_claim_flag,
       c.reopen_date,
       c.closed_date,
       c.fa_objection,
       c.fatal_wcc_num,
       c.claimant_dod,
       c.spanish_forms_emp,
       c.spanish_forms_iw,
       a.surgery_ind,
       ind.first_ind_payment_date, --proxy for date claim when LT
       ct.gross_paid_ind_tt,
       ct.gross_paid_ind_tp,
       ct.gross_paid_ind,
       ct.gross_paid_med,
       n.min_date_of_nurse_ref,
       i.injury_code,
       i.injury_sev_code,
       ad.notification_type_code, -- D1 or D1B for denial DENY_LIABILITY_FLAG is not populated
       ad.mmi_date,
       siu_ref.max_siu_referral,
       siu_ref.min_siu_referral,
       ca.attorney_num,
       ai.ALL_ICD_CODES,
       abc.ALL_BILLED_PROC_CODES
  from claim c
 inner join accident a
    on a.claim_num = c.claim_num
 inner join injury i
    on i.claim_num = c.claim_num
   and i.primary_injury_flag = 'Y'
 inner join claim_totals ct
    on ct.claim_num = c.claim_num
  left join claim_attorney ca
    on ca.claim_num = c.claim_num
   and ca.primary_flag = 'Y'
  left join first_nurse_audit_row n
    on n.claim_num = c.claim_num --This pulls in the non nurse claims
  left join first_indemnity_payment_date ind
    on ind.claim_num = c.claim_num
  left join benefits_notification ad
    on ad.claim_num = c.claim_num
   and ad.current_notification_flag = 'Y'
  left join siu_ref
    on siu_ref.pinnacol_num = c.claim_num
  left join all_icd ai
    on ai.claim_num = c.claim_num
  left join all_billing_codes abc
    on abc.claim_num = c.claim_num
 where extract(year from c.accident_date) > 2009
-- and c.claim_num = 3915818
 order by c.accident_date, c.claim_num
