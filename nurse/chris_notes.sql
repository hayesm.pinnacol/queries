select c.claim_num, c.accident_date,c.nurse_adjuster_code
   , ct.attorney_num, ct.entry_of_app_date
   , a.adjuster_type_code
   , ca.new_value,ca.entry_date nurse_added_date
   , mbs.bill_seq_num, mbs.version_num, mbs.total_billed_amt
   , mbd.service_begin_date 
from claim c
    inner join claim_audit ca on ca.claim_num = c.claim_num and claim_audit_type_code = 'NUR'
	inner join adjuster a on a.adjuster_code = c.adjuster_code where adjuster_type_code in ('NURSE','URRN','CATRN','TRGRN')
    inner join med_bill_summary mbs on mbs.claim_num = c.claim_num and mbs.current_flag = 'Y'
    inner join med_bill_detail mbd on mbd.bill_seq_num = mbs.bill_seq_num and mbd.version_num = mbs.version_num and mbd.bill_line_num = 1
--AND adjuster_type_code in ('NURSE','URRN','CATRN','TRGRN','EIRN','DRRN')
--and ca.claim_audit_type_code = 'NUR'
--and mbs.total_billed_amt >= 5000
--and mbs.current_flag = 'Y'
and extract( year from c.accident_date) > 2009
and ca.entry_date = (select min(entry_date) from claim_audit ca2 where ca2.claim_num = ca.claim_num and ca2.claim_audit_type_code = ca2.claim_audit_type_code)
and mbd.bill_line_num = 1 



1. fix join with claim_attorney - confirm that the outer join works 
2. send describes of all the tables to chris.



/*
  This query returns claims that have been assigned to a nurse.  Because there can be multiple referrals to a nurse, we are looking 
  for the earliest date that a nurse referral happened. Also we are only looking for specific nurse types because every claim is assigned 
  a nurse, but normally it is the claims rep.  
  
  CATRN	Catastrophic Nurse
  DRRN	DELAYED RECOVERY NURSE  
  EIRN	EARLY INTERVENTION NURSE
  TRGRN	Triage Nurse Case Manager
  URRN	UR Nurse

  This is also returning the total bill amount for the current version of each
  medical bill for the claim.  The medical bill line items are included to get the service_begin_date.  The medical bill lines are limited to
  the first line item because the service_date is the same for each detail. The claim attorney is left joined in to indicate if the
  claim is represented.  The ANSI style join is used to allow the left join to be qualified to only return primary attorneys.  
  Claim_num, accident_date, min_date_of_nurse_ref, attorney_num and primary_flag are redundent because of the inclusion of medical bill data.
  for example 
  3966447 has 309 bills 
  3959900 has 18 bills 
  This can be changed to two data sets if preferred.  
  
*/
with first_nurse_audit_row as (
    select min(entry_date) as min_date_of_nurse_ref, claim_num 
    from claim_audit ca 
        ,adjuster a 
    where ca.new_value = a.adjuster_code
    and a.adjuster_type_code in  ('URRN','CATRN','TRGRN','EIRN','DRRN') 
    and claim_audit_type_code = 'NUR'  
    group by claim_num 
)
select c.claim_num
     , c.accident_date
     , n.min_date_of_nurse_ref
     , ca.attorney_num
     , ca.primary_flag
     , b.bill_seq_num
     , b.version_num 
     , b.total_billed_amt
     , d.service_begin_date
from claim c
     inner join first_nurse_audit_row n on n.claim_num = c.claim_num
     inner join med_bill_summary b on b.claim_num = c.claim_num and b.current_flag = 'Y'
     inner join med_bill_detail d on d.bill_seq_num = b.bill_seq_num and d.version_num = b.version_num and d.bill_line_num = 1
     left join claim_attorney ca on ca.claim_num = c.claim_num and ca.primary_flag = 'Y'
  --where c.claim_num in (3966447, 3959900)
  where extract( year from c.accident_date) = 2010
  order by c.claim_num --,d.service_begin_date


2010 410 claims = 51252 rows for each bill 
2018 3837 claims = 251437 rows 
   for each bill with bills over 5000 = 6655 
   for each bill with bills over 5000 = 551 prior to the nurse referal  