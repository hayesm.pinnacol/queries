/*
Hi Melissa,

Oguz and I are working on the MCM escalation model, and there were a few things we were not getting right in our query.  
If you are able to help, it would be greatly appreciated.  

1)  I am using (ACCIDENT_DATE) � (�MIN_DATE_OF_NURSE_REFERRAL�) to determine the number of days between a claim coming in and MCM referral. 
     Using CLAIM_COMPLETE_DATE instead of ACCIDENT_DATE would be helpful.  
     That is the category that pulls up when I use  Help in WCIS on the (Created:) tab in CL085S.
2)  In the last 18 months, MCM referral�s changed a bit, with some new referral types. 
     In Cl800S under (Nurse Type: ) there are 5 types  of MCM�s (CAT, DP, DR, EI, UR). Is there a was to pull the total
     number of times per claim that a MCM referral was made and include the type of referral each time that occurs?
--referral_assigned_user_type
--	URRN	UR Nurse (UR - URRN)
--	CATRN	Catastrophic Nurse (CAT - CATRN) 
--	EIRN	EARLY INTERVENTION NURSE (EI - EIRN)
--	DRRN	DELAYED RECOVERY NURSE (DR DRRN) 
--	TRGRN	Triage Nurse Case Manager (TRGRN) 
--  NURSE NURSE

3)  What is the 'SURGERY_IND' code?  Do you know hoe that is generated or what that is linked to in WCIS?

Below is the Query we have been using for our model build.

Thanks for your help if you are able!

Thanks, 
*/
with all_icd as
                     (select    mbs.claim_num claim_num,
                                substr(listagg(NVL(mbs.ICD9_CODE1, '') || ' ' || NVL(mbs.ICD9_CODE2, '') || ' ' ||
                                     NVL(mbs.ICD9_CODE3, '') || ' ' || NVL(mbs.ICD9_CODE4, ''),
                                     ', ') WITHIN GROUP(ORDER BY mbs.bill_seq_num),1,3999) as ALL_ICD_CODES,
                                min(mbs.RECV_DATE) as min_icd_date
                        from    med_bill_summary mbs, claim c
                       where    mbs.claim_num = c.claim_num
                         and    mbs.current_flag = 'Y'
                         and    mbs.enter_date <= c.accident_date + 30
                         and    mbs.enter_date <= (select min(action_plan_date) from rn_action_plan r where r.claim_num = c.claim_num group by c.claim_num)
                       group by mbs.claim_num)
,
all_billing_codes as
                     (select    mbs.claim_num claim_num,
                                substr(listagg(mbd.billed_procedure_prefix_code ||
                                     mbd.billed_procedure_code || ',') WITHIN GROUP(ORDER BY mbd.detail_seq_num),1,3999) as ALL_BILLED_PROC_CODES,
                                min(mbs.RECV_DATE) as min_cpt_date
                        from    med_bill_summary mbs, med_bill_detail mbd, claim c
                       where    mbs.bill_seq_num = mbd.bill_seq_num
                         and    mbs.version_num = mbd.version_num
                         and    mbs.claim_num = c.claim_num
                         and    mbs.enter_date <= c.accident_date + 30
                         and    mbs.enter_date <= (select min(action_plan_date) from rn_action_plan r where r.claim_num = c.claim_num group by c.claim_num)
                         and    mbs.current_flag = 'Y'
                       group by mbs.claim_num)
,
first_nurse_audit_row as
                     (select    min(action_plan_date) as min_date_of_nurse_ref, r.claim_num
                        from    rn_action_plan r, claim c
                       where    r.claim_num = c.claim_num
                       group by r.claim_num)
,
first_indemnity_payment_date as
                     (select    min(trans_date) as first_ind_payment_date, cft.claim_num
                        from    claim_financial_trans cft
                       group by cft.claim_num)
,
siu_ref as
                     (select    siu.pinnacol_num,
                                min(siu.create_date) min_siu_referral,
                                max(siu.create_date) max_siu_referral,
                                count(*) num_siu_referrals
                        from    siu_referral siu
                       group by siu.pinnacol_num)
,
eoa as              (SELECT     cat.CLAIM_NUM,
                                min(cat.CREATE_DATE) min_ca_date_primary
                     FROM       CLAIM_ATTORNEY cat
                     WHERE      cat.PRIMARY_FLAG = 'Y'
                     GROUP BY   cat.CLAIM_NUM), 
count_eirn as
                     (select    count(*) as count_eirn, r.claim_num
                        from    rn_action_plan r
                        where   r.referral_assigned_user_type = 'EIRN'
                       group by r.claim_num)
,
count_urrn as
                     (select    count(*) as count_urrn, r.claim_num
                        from    rn_action_plan r
                        where   r.referral_assigned_user_type = 'URRN'
                       group by r.claim_num)
,
count_catrn as
                     (select    count(*) as count_catrn, r.claim_num
                        from    rn_action_plan r
                        where   r.referral_assigned_user_type = 'CATRN'
                       group by r.claim_num)
,
count_drrn as
                     (select    count(*) as count_drrn, r.claim_num
                        from    rn_action_plan r
                        where   r.referral_assigned_user_type = 'DRRN'
                       group by r.claim_num)
,
count_trgrn as
                     (select    count(*) as count_trgrn, r.claim_num
                        from    rn_action_plan r
                        where   r.referral_assigned_user_type = 'TRGRN'
                       group by r.claim_num)
,
count_nurse as
                     (select    count(*) as count_nurse, r.claim_num
                        from    rn_action_plan r
                        where   r.referral_assigned_user_type = 'NURSE'
                       group by r.claim_num)                     
select  -- CLAIM --
       c.claim_num,
       c.accident_date,
       c.claim_type_code,
       c.claimant_sex,
       c.claimant_dob,
       c.claimant_language_code,
       c.claimant_language_other,
       c.nlt_to_lt_date,
       c.nlt_to_lt_reason_code,
       c.black_lung_flag,
       c.deny_liability_flag,
       c.severity_code,
       c.garnishment_flag,
       c.workers_claim_flag,
       c.reopen_date,
       c.closed_date,
       c.fa_objection,
       c.fatal_wcc_num,
       c.claimant_dod,
       c.spanish_forms_emp,
       c.spanish_forms_iw,
       a.surgery_ind,
       ind.first_ind_payment_date, --proxy for date claim when LT
       ct.gross_paid_ind_tt,
       ct.gross_paid_ind_tp,
       ct.gross_paid_ind,
       ct.gross_paid_med,
       i.injury_code,
       i.injury_sev_code,
      ad.notification_type_code, -- D1 or D1B for denial DENY_LIABILITY_FLAG is not populated
       ad.mmi_date,
       extract(year from c.accident_date) - extract(year from c.claimant_dob) AS CLAIMANT_AGE,
       n.min_date_of_nurse_ref,
       CASE WHEN n.min_date_of_nurse_ref IS NULL
       THEN 0
       ELSE 1
       END AS nurse_referral,
       CASE WHEN n.min_date_of_nurse_ref IS NULL
       THEN 99999
       ELSE n.min_date_of_nurse_ref - c.claim_complete_date 
       END AS days_to_nurse_referral,
       siu_ref.max_siu_referral,
       siu_ref.min_siu_referral,
       CASE WHEN siu_ref.min_siu_referral IS NULL
       THEN 0
       ELSE 1
       END AS siu_referral,
       CASE WHEN siu_ref.min_siu_referral IS NULL
       THEN 99999
       ELSE siu_ref.min_siu_referral - c.accident_date
       END AS days_to_siu_referral,
       eoa.min_ca_date_primary as eoa_date,
       CASE WHEN eoa.min_ca_date_primary IS NULL
       THEN 0
       ELSE 1
       END AS eoa_referral,
       CASE WHEN eoa.min_ca_date_primary IS NULL
       THEN 99999
       ELSE eoa.min_ca_date_primary - c.accident_date
       END AS days_to_eoa_referral,
       ca.attorney_num,
       ai.ALL_ICD_CODES,
       CASE WHEN ai.ALL_ICD_CODES IS NULL
       THEN 0
       ELSE 1
       END AS has_icd_codes,
       CASE WHEN ai.ALL_ICD_CODES IS NULL
       THEN NULL
       ELSE ai.min_icd_date
       END AS min_icd_date,
       abc.ALL_BILLED_PROC_CODES,
       CASE WHEN abc.ALL_BILLED_PROC_CODES IS NULL
       THEN 0
       ELSE 1
       END AS has_cpt_codes,
       CASE WHEN abc.ALL_BILLED_PROC_CODES IS NULL
       THEN NULL
       ELSE abc.min_cpt_date
       END AS min_cpt_date,-- SOURCES --    
       ce.count_eirn,
       cu.count_urrn,
       cc.count_catrn,
       cd.count_drrn,
       ct.count_trgrn,
       cn.count_nurse 
  from claim c
inner join accident a
    on a.claim_num = c.claim_num
inner join injury i
    on i.claim_num = c.claim_num
   and i.primary_injury_flag = 'Y'
inner join claim_totals ct
    on ct.claim_num = c.claim_num
  left join claim_attorney ca
    on ca.claim_num = c.claim_num
   and ca.primary_flag = 'Y'
  left join first_nurse_audit_row n
    on n.claim_num = c.claim_num
  left join first_indemnity_payment_date ind
    on ind.claim_num = c.claim_num
  left join benefits_notification ad
    on ad.claim_num = c.claim_num
   and ad.current_notification_flag = 'Y'
  left join siu_ref
    on siu_ref.pinnacol_num = c.claim_num
  left join eoa
    on eoa.claim_num = c.claim_num
  left join all_icd ai
    on ai.claim_num = c.claim_num
  left join all_billing_codes abc
    on abc.claim_num = c.claim_num--Filter
  left join count_eirn ce 
    on ce.claim_num = c.claim_num       
  left join count_urrn cu 
    on cu.claim_num = c.claim_num       
  left join count_catrn cc 
    on cc.claim_num = c.claim_num     
  left join count_drrn cd 
    on cd.claim_num = c.claim_num          
  left join count_trgrn ct 
    on ct.claim_num = c.claim_num       
  left join count_nurse cn 
    on cn.claim_num = c.claim_num           
where extract(year from c.accident_date) > 2009
AND c.claimant_sex IS NOT NULL
AND extract(year from c.accident_date) - extract(year from c.claimant_dob) IS NOT NULL
AND i.injury_code IS NOT NULL
AND i.injury_sev_code IS NOT NULL
AND c.claimant_language_code IS NOT NULL
AND a.surgery_ind IS NOT NULL
AND c.claim_num = 10058404
-- AND ROWNUM < 100
order by c.accident_date, c.claim_num;

