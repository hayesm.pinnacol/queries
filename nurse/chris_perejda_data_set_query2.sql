/*
  This query returns claims that have been assigned to a nurse.  Because there can be multiple referrals to a nurse, we are looking 
  for the earliest date that a nurse referral happened. We are using the rn referral system to find the first referral to a nurse.

  This is also returning the total bill amount for the current version of each bill that was greater that 5000 and occurred before the nurse referral.
  The medical bill line details are included to get the service_begin_date.  The medical bill lines are limited to
  the first line item because the service_date is the same for each detail. The claim attorney is left joined to indicate if the
  claim is represented.  The ANSI style join is used to allow the left join to be qualified to only return primary attorneys.  
  Claim_num, accident_date, min_date_of_nurse_ref, attorney_num and primary_flag are redundent because of the inclusion of medical bill data.
  If there are more than one bill > 5000 prior to the nurse referral there will be a row for each unique bill_seq_num on the current version of the bill. 
 CLAIMANT_SEX – male or female

CLAIMANT_DOB - age 

CLAIMANT_LANGUAGE_CODE        -either of these should be fine if they are saying the same thing 
CLAIMANT_LANGUAGE_OTHER

NLT_TO_LT_REASON_CODE         I am assuming this has some text for a claim changing from non lost time.

BLACK_LUNG_FLAG               If this is a yes, there is a nurse.

CRPS (chronic Regional Pain Syndrome)- I’m not sure that we have a flag for this. (f/u item for Chris with medpay, any billing codes specific to CRPS??)

injury codes-  What injury codes were filed with the claim(would this be a multi-object item, like one row for back, one for shoulder?)

DENY_LIABILITY_FLAG – Was the claim denied?

surgery flag- surgery(is this triggered by a med record or a billing code?)

severe injury flag- how severe was the injury? 

severity code- same as above

NLT_TO_LT_DATE- Would be helpful to identify when a claim became lost time in the lifecycle of the claim.    
                   
NLT_TO_LT_REASON_CODE-  I’m not sure what this looks like exactly   


NLT_SPECIAL_FLAG-  Is this a special code for some issue the PH is reporting when filing the claim, like question the injury, ect??

screen cl086s(count of paid services, not sure if that is just billing codes?)

TTD paid –Total TTD paid(didn’t see anything in the columns about this)  

GARNISHMENT_FLAG – Usually a child support lien

WORKERS_CLAIM_FLAG (does this mean the worker filed the claim with the DOWC?)

TEMP_PAY_END_DATE  (how long were they receiving lwb)

SOURCE_OF_FIRST_REPORT_CODE (who reported this, injured worker, attorney, policy holder)

REOPEN_DATE (did Pinnacol close and have to re-open, newer or older)

FA_OBJECTION  
                          
FATAL_WCC_NUM

SPANISH_FORMS_IW (Is the IW Spanish speaking?)

SPANISH_FORMS_EMP (Is  the PH Spanish speaking?)

OCC_DISEASE_DISABLE_DATE (Is this an occupational disease)

MAX_MED_IMPROVE_DATE (2 measure; how long to MMI and how long after MMI did it take to close the claim?)

INVESTIGATION_STATUS_CODE (is this just telling me if there was an investigative referral?)

CLOSED_DATE  (to determine how long the claim was open and determine several other key date metrics)
    
  
*/
with first_nurse_audit_row as (
    select min(action_plan_date) as min_date_of_nurse_ref, r.claim_num 
    from rn_action_plan r 
        ,claim c 
    where r.claim_num = c.claim_num
    group by r.claim_num 
)
select c.claim_num 
      
     , c.accident_date 
     , n.min_date_of_nurse_ref
     , ca.attorney_num 
	 , ca.primary_flag
     , count(*) over (partition by b.claim_num) as num_of_bills_included 
    , b.total_billed_amt , d.service_begin_date
from claim c
     inner join first_nurse_audit_row n on n.claim_num = c.claim_num
     inner join med_bill_summary b on b.claim_num = c.claim_num and b.current_flag = 'Y' and b.enter_date <= n.min_date_of_nurse_ref and b.total_billed_amt > =5000
     inner join med_bill_detail d on d.bill_seq_num = b.bill_seq_num and d.version_num = b.version_num and d.bill_line_num = 1 
     left join claim_attorney ca on ca.claim_num = c.claim_num and ca.primary_flag = 'Y'
  where extract( year from c.accident_date) > 2009
  order by c.accident_date