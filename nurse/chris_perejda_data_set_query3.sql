/*

Version 1
  This query returns claims in the last 10 years with the following data if it applys to the claim:
       earliest nurse referral date
       medical bill total_billed_amt and service_begin_date up to the first nurse referral data if there is a nurse referral date
       all medical bill total_billed_amt and service_begin_date for claims with no nurse referral
       primary claim attorney numbers
       
  
  Because there can be multiple referrals to a nurse, we are looking for the earliest date that a nurse referral happened. 
  We are using the rn referral system to find the first referral to a nurse.

Version 2 
  Added the following columns claimant_sex,claimant_dob,claimant_dod,claimant_language_code,,
  nlt_to_lt_date,nlt_to_lt_reason_code,black_lung_flag, injury codes, severity_code, gross_paid_ind_ttd, gross_paid_ind_tpd, 
  garnishment_flag, workers_claim_flag, reopen_date, fa_objection, fatal_wcc_num, claimant_dod, spanish_forms_iw, spanish_forms_ph,
  closed_date  
  
* surgery_flag (from accident, not sure it is updated when surgery occurs)  
* Using first indemnity payment as a proxy for when the claim becomes LT 
* CRPS (chronic Regional Pain Syndrome)- I�m not sure that we have a flag for this. (f/u item for Chris with medpay, any billing codes specific to CRPS??)
* deny_liability_flag is no longer populated - added admission notification code (D1 or D1b for denial) 
* severe_injury_flag- how severe was the injury? severe_injury_flag is not populated 
* nlt_special_flag - this field is not populated 
* screen cl086s(count of paid services, not sure if that is just billing codes?) These were not included.  This is a count of massages, OTPT, Accupuncture and Chiropractic.  It is based on the medical managment tree.      
* temp_pay_end_date -this is not populated   (how long were they receiving lwb) 
* occ_disease_disable_date this is not populated  (Is this an occupational disease) 
* max_med_improve_date- this was not populated but added mmi from benefits_notification   
* investigation_status_code - this is not populated but I addded first and last siu_referral_date   

A separate query was made for bills.  This data can be joined by claim number. 

*/
--Bill query.  This query returns bill up to the first nurse referral 
with first_nurse_audit_row as
 (select min(action_plan_date) as min_date_of_nurse_ref, r.claim_num
    from rn_action_plan r, claim c
   where r.claim_num = c.claim_num
   group by r.claim_num)
 select c.claim_num,
       c.accident_date,
       n.min_date_of_nurse_ref,
       count(*) over(partition by b.claim_num) as num_of_bills_included,
       b.total_billed_amt,
       d.service_begin_date
 from claim c
 inner join med_bill_summary b
    on b.claim_num = c.claim_num
   and b.current_flag = 'Y'
 inner join med_bill_detail d
    on d.bill_seq_num = b.bill_seq_num
   and d.version_num = b.version_num
   and d.bill_line_num = 1
  left join first_nurse_audit_row n
    on n.claim_num = c.claim_num
 where extract(year from c.accident_date) > &year
 and b.enter_date <= n.min_date_of_nurse_ref
 order by c.accident_date, c.claim_num
 
--claim query 
with first_nurse_audit_row as
 (select min(action_plan_date) as min_date_of_nurse_ref, r.claim_num
    from rn_action_plan r, claim c
   where r.claim_num = c.claim_num
   group by r.claim_num),
first_indemnity_payment_date as
 (select min(trans_date) as first_ind_payment_date, cft.claim_num
    from claim_financial_trans cft
   group by cft.claim_num),
siu_ref as
 (select siu.pinnacol_num,
         min(siu.create_date) min_siu_referral,
         max(siu.create_date) max_siu_referral,
         count(*) num_siu_referrals
    from siu_referral siu
   group by siu.pinnacol_num)
select c.claim_num,
       c.accident_date,
       c.claim_type_code,
       c.claimant_sex,
       c.claimant_dob,
       c.claimant_language_code,
       c.claimant_language_other,
       c.nlt_to_lt_date,
       c.nlt_to_lt_reason_code,
       c.black_lung_flag, 
       c.deny_liability_flag,
       c.severity_code,
       c.garnishment_flag,
       c.workers_claim_flag,
       c.reopen_date,
       c.closed_date,
       c.fa_objection,
       c.fatal_wcc_num,
       c.claimant_dod,
       c.spanish_forms_emp,
       c.spanish_forms_iw,
       a.surgery_ind,
       ind.first_ind_payment_date, --proxy for date claim when LT 
       ct.gross_paid_ind_tt, 
       ct.gross_paid_ind_tp,
       ct.gross_paid_ind,
       ct.gross_paid_med,
       n.min_date_of_nurse_ref,
       i.injury_code,
       i.injury_sev_code,
       ad.notification_type_code,  -- D1 or D1B for denial DENY_LIABILITY_FLAG is not populated 
       ad.mmi_date,
              siu_ref.max_siu_referral,
       siu_ref.min_siu_referral,
       ca.attorney_num
  from claim cs
  inner join accident a
    on a.claim_num = c.claim_num
 inner join injury i 
 on i.claim_num = c.claim_num
 and i.primary_injury_flag = 'Y'  
 inner join claim_totals ct
 on ct.claim_num = c.claim_num
  left join claim_attorney ca
    on ca.claim_num = c.claim_num
   and ca.primary_flag = 'Y'
  left join first_nurse_audit_row n
    on n.claim_num = c.claim_num  
  left join first_indemnity_payment_date ind
    on ind.claim_num = c.claim_num
 left join benefits_notification ad
    on ad.claim_num = c.claim_num
   and ad.current_notification_flag = 'Y' 
   left join siu_ref
    on siu_ref.pinnacol_num = c.claim_num 
 where extract(year from c.accident_date) > &year 
  -- and c.claim_num = 3915818
 order by c.accident_date, c.claim_num
