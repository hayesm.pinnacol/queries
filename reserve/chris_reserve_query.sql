-- https://github.pinnacol.com/DataScience/data-tools-package/blob/master/pinaas_data_tools/bill_data_proc/query.py


with all_icd as
(select mbs.claim_num claim_num,
       listagg(NVL(mbs.ICD9_CODE1, '') || ' ' || NVL(mbs.ICD9_CODE2, '') || ' ' ||
               NVL(mbs.ICD9_CODE3, '') || ' ' || NVL(mbs.ICD9_CODE4, ''),
               ', ') WITHIN GROUP(ORDER BY mbs.bill_seq_num) as ALL_ICD_CODES
  from med_bill_summary mbs, claim c
 where mbs.claim_num = c.claim_num
   and mbs.current_flag = 'Y'
   and mbs.enter_date <= c.accident_date + 30
 group by mbs.claim_num),
all_billing_codes as
(select mbs.claim_num claim_num,
       listagg(mbd.billed_procedure_prefix_code ||
               mbd.billed_procedure_code || ',') WITHIN GROUP(ORDER BY mbd.detail_seq_num) as ALL_BILLED_PROC_CODES
  from med_bill_summary mbs, med_bill_detail mbd, claim c
 where mbs.bill_seq_num = mbd.bill_seq_num
   and mbs.version_num = mbd.version_num
   and mbs.claim_num = c.claim_num
   and mbs.enter_date <= c.accident_date + 30
   and mbs.current_flag = 'Y'
 group by mbs.claim_num),
all_injuries as
  (select c.claim_num,
    listagg(NVL(i.injury_code, ': ')||' '||NVL(ic.code_descr, ' ')||' ' )
    within group (order by i.injury_code) as all_injuries
   from claim c, injury i, injury_code ic
   where c.claim_num = i.claim_num
   and i.injury_code = ic.injury_code
   group by i.claim_num)
 select c.claim_num
 ,c.accident_date
 ,ai.all_injuries
 ,ei.iw_smoke
 ,ei.iw_weight_lbs
 ,ei.iw_weight
 ,ei.iw_height
 ,ei.iw_height_ft
 ,ei.iw_height_in
 ,ei.other_medical_conditions
 ,ei.bodyparts_injured
 ,ct.gross_incurred_med
 ,ct.gross_incurred_ind
 ,ct.gross_incurred_exp
 ,ai.ALL_ICD_CODES
 ,abc.ALL_BILLED_PROC_CODES
 from claim c
 inner join claim_totals ct
    on ct.claim_num = c.claim_num
 inner join all_injuries ai
   on ai.claim_num = c.claim_num
 left join iw_ei_checklist ei
    on ei.claim_num = c.claim_num
 left join all_icd ai
    on ai.claim_num = c.claim_num
 left join all_billing_codes abc
    on abc.claim_num = c.claim_num
 where extract(year from c.accident_date) > 2009
-- and c.claim_num in(3744231, 3471318)
