-- https://github.pinnacol.com/DataScience/data-tools-package/blob/master/pinaas_data_tools/bill_data_proc/query.py
--claim_num, est_date, est_amt_medical, est_comp_amt
-- https://github.pinnacol.com/DataScience/data-tools-package/blob/master/pinaas_data_tools/bill_data_proc/query.py
--claim_num, est_date, est_amt_medical, est_comp_amt
/*

--4 min 37 sec 1 claim
select /*+ rule */
       level             lev,
       h.id,
       h.mm_category_id,
       h.parent_id,
       h.hierarchy_id,
       mbs.claim_num,
       mbs.bill_seq_num,
       ppc.*,
       c.*,
       pc.*
  from mm_hierarchy h
     ,mm_procedure_code pc
     ,mm_category c
     ,procedure_code ppc
     ,med_bill_summary mbs
     ,med_bill_detail mbd
where pc.mm_hierarchy_id = h.id
and h.mm_category_id = c.id
and mbs.version_num = mbd.version_num
and mbs.bill_seq_num = mbd.bill_seq_num
and mbd.allowed_procedure_code_seq_num = ppc.procedure_code_seq_num
and pc.thru_date is null
and pc.procedure_code = ppc.procedure_code
and pc.procedure_prefix_code = ppc.procedure_prefix_code
and pc.procedure_type_code = ppc.procedure_type_code
--and ppc.thru_date is null
--and pc.procedure_code =  '00406012301'
and mbs.claim_num = 3945850
connect by prior h.id = h.parent_id
start with parent_id = 2736--0;
*/


with all_icd as
(select mbs.claim_num claim_num,
       listagg(NVL(mbs.ICD9_CODE1, '') || ' ' || NVL(mbs.ICD9_CODE2, '') || ' ' ||
               NVL(mbs.ICD9_CODE3, '') || ' ' || NVL(mbs.ICD9_CODE4, ''),
               ' ') WITHIN GROUP(ORDER BY mbs.bill_seq_num) as ALL_ICD_CODES
  from med_bill_summary mbs, claim c
 where mbs.claim_num = c.claim_num
   and mbs.current_flag = 'Y'
   --and mbs.enter_date <= c.accident_date + 30
 group by mbs.claim_num),
all_billing_codes as
(select mbs.claim_num claim_num,
       listagg(mbd.billed_procedure_prefix_code ||
               mbd.billed_procedure_code||'  ' ) WITHIN GROUP(ORDER BY mbd.detail_seq_num) as ALL_BILLED_PROC_CODES
  from med_bill_summary mbs, med_bill_detail mbd, claim c
 where mbs.bill_seq_num = mbd.bill_seq_num
   and mbs.version_num = mbd.version_num
   and mbs.claim_num = c.claim_num
   --and mbs.enter_date <= c.accident_date + 30
   and mbs.current_flag = 'Y'
 group by mbs.claim_num),
all_reserves as
( select e.claim_num claim_num,
         listagg('{'||e.est_date||' MED INCURRED '||e.est_amt_medical||' IND INCURRD '||e.est_comp_amt||'} ')
            WITHIN GROUP (order by e.est_date) AS ALL_RESERVES
  from estimate e
  group by e.claim_num
  ),
all_injuries as
  (select c.claim_num,
    listagg('{'||NVL(i.injury_code, ' ')||' '||NVL(ic.code_descr, ' ')||'} ' )
    within group (order by i.injury_code) as all_injuries
   from claim c, injury i, injury_code ic
   where c.claim_num = i.claim_num
   and i.injury_code = ic.injury_code
   group by i.claim_num)
 select c.claim_num
 ,c.accident_date
 ,c.claimant_city
 ,c.claimant_zip
 ,c.claimant_dob
 ,c.claim_enter_date
 ,c.claim_status_code
 ,c.claim_closed_date
 ,(select loc_city from location where loc_seq_num = sf_cl085s_bus_loc(c.policy_num,c.bus_seq_num,c.loc_seq_num,sysdate)) loc_city
 ,(select loc_zip from location where loc_seq_num = sf_cl085s_bus_loc(c.policy_num,c.bus_seq_num,c.loc_seq_num,sysdate)) loc_zip
 ,froi.date_hired
 ,froi.administrator_notify_date
 ,froi.accident_cause_code
 ,froi.accident_nature_code accident_source_code
 ,froi.emplyr_notified_date
 ,froi.prepared_date
 ,froi.wage_rate
 ,froi.wage_rate_code
 ,froi.days_worked_per_week
 ,froi.hours_per_day
 ,froi.hours_per_week
 ,froi.manual_class_num
 ,froi.manual_class_type_code
 ,pk80340_benefits_functions.get_aww_rate(c.claim_num)
 ,ai.all_injuries
 ,ct.gross_incurred_med
 ,ct.gross_incurred_ind
 ,ct.gross_incurred_exp
 ,ca.entry_of_app_date
 ,ar.ALL_RESERVES
 ,ai.ALL_ICD_CODES
 ,abc.ALL_BILLED_PROC_CODES
 from claim c
 inner join first_report_of_injury froi
   on froi.claim_num = c.claim_num
 inner join claim_totals ct
    on ct.claim_num = c.claim_num
 inner join all_injuries ai
   on ai.claim_num = c.claim_num
 inner join accident a
     on a.claim_num = c.claim_num
 left join claim_attorney ca
    on ca.claim_num = c.claim_num
    and ca.primary_flag = 'Y'
 left join all_icd ai
    on ai.claim_num = c.claim_num
 left join all_billing_codes abc
    on abc.claim_num = c.claim_num
 left join all_reserves ar
    on ar.claim_num = c.claim_num
 where extract(year from c.accident_date) > 2009
 and c.claim_num in(3744231, 3471318)
