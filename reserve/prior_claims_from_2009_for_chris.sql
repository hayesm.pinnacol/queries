
SELECT VAL_T.VALUATION_DATE,
  --    clm.date_key,
  --    t.date_key,
     CLM.CLAIM_NUM,
     clm.accident_date,
     clm.adjusted_incurred_ind,
     clm.adjusted_incurred_med,
     prclm.prior_claim_num
  FROM C3P_OWNER.PRIOR_CLAIMS_MAP    PRCLM,
     C3P_OWNER.CLAIM_FACT        CLM,
     C3P_OWNER.VALUATION_TIME_DIM  VAL_T,
     C3P_OWNER.TIME_DIM            T
   WHERE (CLM.CLAIM_NUM = PRCLM.BASE_CLAIM_NUM (+)
       AND CLM.DATE_KEY = VAL_T.DATE_KEY
       AND CLM.ACCIDENT_DATE = T.DATE_KEY)
       AND ((VAL_T.CURRENT_MONTH_END_DATE_FLAG = 1
           AND T.YEAR BETWEEN 2009 AND 2020))
        --AND rownum < 50
       --and clm.claim_num in (3951855,3889910,3579754,3534652,3333377) -- (3943655,3947467,3951940,3951855)
       order by clm.accident_date desc, clm.claim_num 
