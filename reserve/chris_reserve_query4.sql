with all_icd1 as
(select mbs.claim_num claim_num,
       substr(listagg(NVL(mbs.ICD9_CODE1, ''),
               ' ') WITHIN GROUP(ORDER BY mbs.claim_num),1,3800) as ALL_ICD_CODES1
  from (select distinct mbs.claim_num, icd9_code1
        from med_bill_summary mbs
              ,claim c
         where mbs.claim_num = c.claim_num
         and mbs.current_flag = 'Y'
         and mbs.enter_date <= c.accident_date + 30) mbs
         group by mbs.claim_num
         ),
all_icd2 as
(select mbs.claim_num claim_num,
       substr(listagg(NVL(mbs.ICD9_CODE2, ''),
               ' ') WITHIN GROUP(ORDER BY mbs.claim_num),1,3800) as ALL_ICD_CODES2
  from (select distinct mbs.claim_num, icd9_code2
            from med_bill_summary mbs
              ,claim c
         where mbs.claim_num = c.claim_num
         and mbs.current_flag = 'Y'
         and mbs.enter_date <= c.accident_date + 30) mbs
         group by mbs.claim_num
         ),
all_icd3 as
(select mbs.claim_num claim_num,
       substr(listagg(NVL(mbs.ICD9_CODE3, ''),
               ' ') WITHIN GROUP(ORDER BY mbs.claim_num),1,3800) as ALL_ICD_CODES3
  from (select distinct mbs.claim_num, icd9_code3
            from med_bill_summary mbs
              ,claim c
         where mbs.claim_num = c.claim_num
         and mbs.current_flag = 'Y'
         and mbs.enter_date <= c.accident_date + 30) mbs
         group by mbs.claim_num
         ),
all_icd4 as
(select mbs.claim_num claim_num,
       substr(listagg(NVL(mbs.ICD9_CODE4, ''),
               ' ') WITHIN GROUP(ORDER BY mbs.claim_num),1,3800) as ALL_ICD_CODES4
  from (select distinct mbs.claim_num, icd9_code4
            from med_bill_summary mbs
              ,claim c
          where mbs.claim_num = c.claim_num
          and mbs.current_flag = 'Y'
          and mbs.enter_date <= c.accident_date + 30) mbs
         group by mbs.claim_num
         ),
all_billing_codes as
(select mbs.claim_num claim_num,
       substr(listagg(mbd.billed_procedure_prefix_code ||
               mbd.billed_procedure_code||'  ')  WITHIN GROUP(ORDER BY mbd.detail_seq_num),1,3800) as ALL_BILLED_PROC_CODES
  from med_bill_summary mbs, med_bill_detail mbd, claim c
 where mbs.bill_seq_num = mbd.bill_seq_num
   and mbs.version_num = mbd.version_num
   and mbs.claim_num = c.claim_num
   and mbd.allowed_procedure_type_code != 'NDC'
   and mbs.enter_date <= c.accident_date + 30
   and mbs.current_flag = 'Y'
 group by mbs.claim_num),

all_ndc_codes as
(select mbs.claim_num claim_num,
       substr(listagg(mbd.billed_procedure_code||'  ')  WITHIN GROUP(ORDER BY mbd.detail_seq_num),1,3800) as ALL_NDC_CODES
  from med_bill_summary mbs, med_bill_detail mbd, claim c
 where mbs.bill_seq_num = mbd.bill_seq_num
   and mbs.version_num = mbd.version_num
   and mbs.claim_num = c.claim_num
   and mbd.allowed_procedure_type_code = 'NDC'
   and mbs.enter_date <= c.accident_date + 90
   and mbs.current_flag = 'Y'
 group by mbs.claim_num),
all_reserves as
( select e.claim_num claim_num,
         listagg('{'||e.est_date||' MED INCURRED '||e.est_amt_medical||' IND INCURRED '||e.est_comp_amt||'} ')
            WITHIN GROUP (order by e.est_date) AS ALL_RESERVES
  from estimate e
  group by e.claim_num
  ),
all_injuries as
  (select c.claim_num,
    listagg('{'||NVL(i.injury_code, ' ')||' '||NVL(ic.code_descr, ' ')||'} ' )
    within group (order by i.injury_code) as all_injuries
   from claim c, injury i, injury_code ic
   where c.claim_num = i.claim_num
   and i.injury_code = ic.injury_code
   group by i.claim_num)
 select c.claim_num
 ,c.claim_status_code
 ,c.accident_date
 ,c.claimant_city
 ,c.claimant_zip
 ,c.claimant_dob
 ,(select loc_city from location where loc_seq_num = sf_cl085s_bus_loc(c.policy_num,c.bus_seq_num,c.loc_seq_num,sysdate)) loc_city
 ,(select loc_zip from location where loc_seq_num = sf_cl085s_bus_loc(c.policy_num,c.bus_seq_num,c.loc_seq_num,sysdate)) loc_zip
 ,froi.date_hired
 ,froi.administrator_notify_date
 ,froi.accident_cause_code
 ,froi.accident_nature_code accident_source_code
 ,froi.emplyr_notified_date
 ,froi.prepared_date
 ,froi.wage_rate
 ,froi.wage_rate_code
 ,froi.days_worked_per_week
 ,froi.hours_per_day
 ,froi.hours_per_week
 ,froi.manual_class_num
 ,froi.manual_class_type_code
 ,pk80340_benefits_functions.get_aww_rate(c.claim_num)
 ,ai.all_injuries
 ,ct.gross_incurred_med
 ,ct.gross_incurred_ind
 ,ct.gross_incurred_exp
 ,ca.entry_of_app_date
  ,ai1.ALL_ICD_CODES1
 ,ai2.ALL_ICD_CODES2
 ,ai3.ALL_ICD_CODES3
 ,ai4.ALL_ICD_CODES4
 ,abc.ALL_BILLED_PROC_CODES
 ,anc.all_ndc_codes
 ,ar.ALL_RESERVES
 from claim c
 inner join first_report_of_injury froi
   on froi.claim_num = c.claim_num
 inner join claim_totals ct
    on ct.claim_num = c.claim_num
 inner join all_injuries ai
   on ai.claim_num = c.claim_num
 inner join accident a
     on a.claim_num = c.claim_num
 left join claim_attorney ca
    on ca.claim_num = c.claim_num
    and ca.primary_flag = 'Y'
 left join all_icd1 ai1
    on ai1.claim_num = c.claim_num
  left join all_icd2 ai2
    on ai2.claim_num = c.claim_num
 left join all_icd3 ai3
    on ai3.claim_num = c.claim_num
  left join all_icd4 ai4
    on ai4.claim_num = c.claim_num
 left join all_billing_codes abc
    on abc.claim_num = c.claim_num
 left join all_ndc_codes anc
    on anc.claim_num = c.claim_num
 left join all_reserves ar
    on ar.claim_num = c.claim_num
 where extract(year from c.accident_date) > 2009
-- and c.claim_num in(3744231,3945850, 3471318)
